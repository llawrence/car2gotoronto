<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>car2go Portland | Portland Car Sharing | Rideshare In Portland</title>
<meta name="description" content="Car sharing has come to town; Portland car2go means car rental by the minute for modern urban mobility. "/>
<meta id="MetaKeywords" name="KEYWORDS" content=" car2go Portland, Portland car2go, car to go, car rental, Portland car rental" />
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<meta http-equiv="PRAGMA" content="NO-CACHE">
<meta http-equiv="Expires" content="0">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="imagetoolbar" content="no">
<link rel="shortcut icon" href="http://www.car2go.com/favicon.ico" type="image/x-icon">
<link rel="Stylesheet" type="text/css" href="style.css" title="Menu Styles">
<link rel="icon" href="http://www.car2go.com/favicon.ico" type="image/x-icon">

<script src="SpryAssets/SpryEffects.js" type="text/javascript"></script>
<script type="text/javascript">

<!--
function MM_validateForm() { //v4.0
  if (document.getElementById){
    var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
    for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=document.getElementById(args[i]);
      if (val) { nm=val.name; if ((val=val.value)!="") {
        if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
          if (p<1 || p==(val.length-1)) errors+='- '+nm+' must contain an e-mail address.\n';
        } else if (test!='R') { num = parseFloat(val);
          if (isNaN(val)) errors+='- '+nm+' must contain a number.\n';
          if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
            min=test.substring(8,p); max=test.substring(p+1);
            if (num<min || max<num) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
      } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' is required.\n'; }
    } if (errors) alert('The following error(s) occurred:\n'+errors);
    document.MM_returnValue = (errors == '');
} }
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
function MM_effectAppearFade(targetElement, duration, from, to, toggle)
{
	Spry.Effect.DoFade(targetElement, {duration: duration, from: from, to: to, toggle: toggle});
}


<!--
var swf ='<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0" width="321" height="275"><param name="movie" value="car2go-HowItWorks2.swf"><param name="quality" value="high"><embed src="car2go-HowItWorks2.swf" width="321" height="275" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash"></embed></object>';

function flashSwap() 
{
	document.getElementById("movie").innerHTML = swf;
}
//-->
</script>
<style type="text/css" media="screen">
#flashContent {
	width:100%;
	height:100%;
}
</style>

<script src="http://www.apple.com/library/quicktime/scripts/ac_quicktime.js" language="JavaScript" type="text/javascript"></script>
<script src="http://www.apple.com/library/quicktime/scripts/qtp_library.js" language="JavaScript" type="text/javascript"></script>
<link href="http://www.apple.com/library/quicktime/stylesheets/qtp_library.css" rel="StyleSheet" type="text/css" />

</head>
<body id="index" onload="MM_preloadImages('imgs/simple-nav_s2.jpg','imgs/convenient-nav_s2.jpg','imgs/affordable-nav_s2.jpg','imgs/sustainable-nav_s2.jpg','imgs/member-link_s2.jpg','imgs/drive-nav_s2.jpg')">
<div align="center"><img src="https://secure.fastclick.net/w/tre?ad_id=24878;evt=17776;cat1=22350;cat2=22351;rand=[CACHEBUSTER]" width="1" height="1" border="0">
  <table border="0" cellpadding="5" cellspacing="0" width="950">
    <tr>
      <td width="675">&nbsp;</td>
      <td align="right" valign="bottom"><iframe src="http://www.facebook.com/plugins/like.php?app_id=191729397539808&amp;href=http%3A%2F%2Fwww.facebook.com%2Fcar2go.austin&amp;send=false&amp;layout=button_count&amp;width=100&amp;show_faces=true&amp;action=like&amp;colorscheme=light&amp;font=arial&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:100px; height:21px;" allowTransparency="true"></iframe>
        
        <!-- AddThis Button BEGIN -->
        
    <?php require_once('add_this.php'); ?>
        <!-- AddThis Button END --></td>
    </tr>
  </table>
  <table border="0" cellpadding="0" cellspacing="0" width="950">
    <tr>
      <td width="21" height="21"><img src="imgs/corner_left_top.png" width="21" height="21" alt="Portland Car Sharing"></td>
      <td bgcolor="#ffffff" width="908"></td>
      <td width="21" height="21"><img src="imgs/corner_right_top.png" width="21" height="21" alt="Portland Car Sharing"></td>
    </tr>
    <tr>
      <td width="21" bgcolor="#FFFFFF"></td>
      <td bgcolor="#ffffff" width="908"><table style="display: inline-table;" bgcolor="#009bda" border="0" cellpadding="0" cellspacing="0" width="908">
          <tr bgcolor="#FFFFFF">
            <td valign="top"><a href="index.php"><img name="car2gologo_s1" src="imgs/car2go-logo_s1.jpg" width="136" height="95" border="0" id="car2gologo_s1" alt="" /></a><br /></td>
            <td valign="top"><a href="simple.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('simplenav_s1','','imgs/simple-nav_s2.jpg',1);"><img name="simplenav_s1" src="imgs/simple-nav_s1.jpg" width="142" height="95" border="0" id="simplenav_s1" alt="" /></td>
            <td valign="top"><a href="convenient.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('convenientnav_s1','','imgs/convenient-nav_s2.jpg',1);"><img name="convenientnav_s1" src="imgs/convenient-nav_s1.jpg" width="145" height="95" border="0" id="convenientnav_s1" alt="" /></a></td>
            <td valign="top"><a href="affordable.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('affordablenav_s1','','imgs/affordable-nav_s2.jpg',1);"><img name="affordablenav_s1" src="imgs/affordable-nav_s1.jpg" width="145" height="95" border="0" id="affordablenav_s1" alt="" /></a></td>
            <td valign="top"><a href="sustainable.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('sustainablenav_s1','','imgs/sustainable-nav_s2.jpg',1);"><img name="sustainablenav_s1" src="imgs/sustainable-nav_s1.jpg" width="145" height="95" border="0" id="sustainablenav_s1" alt="" /></a></td>
            <td valign="top" align="right" bgcolor="#FFFFFF">&nbsp;</td>
          </tr>
        </table></td>
      <td width="21" bgcolor="#FFFFFF"></td>
    </tr>
    <tr valign="top">
      <td width="950" colspan=3 valign="top" bgcolor="#ffffff"><div id="flashContent">
          <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="950" height="265" id="vancouver-homecar" align="top">
            <param name="movie" value="vancouver-homecar.swf" />
            <param name="quality" value="high" />
            <param name="bgcolor" value="#ffffff" />
            <param name="play" value="true" />
            <param name="loop" value="true" />
            <param name="wmode" value="transparent" />
            <param name="scale" value="showall" />
            <param name="menu" value="true" />
            <param name="devicefont" value="false" />
            <param name="salign" value="" />
            <param name="allowScriptAccess" value="sameDomain" />
            <!--[if !IE]>-->
            <object type="application/x-shockwave-flash" data="vancouver-homecar.swf" width="950" height="265">
              <param name="movie" value="vancouver-homecar.swf" />
              <param name="quality" value="high" />
              <param name="bgcolor" value="#ffffff" />
              <param name="play" value="true" />
              <param name="loop" value="true" />
              <param name="wmode" value="transparent" />
              <param name="scale" value="showall" />
              <param name="menu" value="true" />
              <param name="devicefont" value="false" />
              <param name="salign" value="" />
              <param name="allowScriptAccess" value="sameDomain" />
              <!--<![endif]--> 
              <img src="imgs/static-background.jpg" width="950" height="378" alt="car2go"> 
              <!--[if !IE]>-->
            </object>
            <!--<![endif]-->
          </object>
        </div></td>
    </tr>
    <tr valign="top">
      <td width="950" colspan=3 valign="top" bgcolor="#ffffff"><div class="padtop padbot">
          <table cellpadding="0" cellspacing="0" border="0" width="950">
            <tr>
              <td width="21" bgcolor="#FFFFFF"></td>
              <td valign="top"><a href="member.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('memberlink_s1','','imgs/member-link_s2.jpg',1);"><img name="memberlink_s1" src="imgs/member-link_s1.jpg" width="453" height="73" border="0" id="memberlink_s1" alt="" /></a></td>
              <td valign="top"><a href="drive.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('drivenav_s1','','imgs/drive-nav_s2.jpg',1);"><img name="drivenav_s1" src="imgs/drive-nav_s1.jpg" width="455" height="73" border="0" id="drivenav_s1" alt="" /></a></td>
              <td width="21" bgcolor="#FFFFFF"></td>
            </tr>
          </table>
        </div></td>
    </tr>
    <tr valign="middle" bgcolor="#E3F5FC">
      <td>&nbsp;</td>
      <td align="center" class="padtop"><table width="440" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td valign="top" width="440"><div class="topinset">
                <div class="bottominset">
                  <h3>Sign up today and become a member.</h3> 
                  <h4><br>
                  </h4>
                  <table width="80%" border="0" align="center" cellpadding="10" cellspacing="0" bgcolor="#eeeeee">
                    <tr>
                      <td align="center" style="text-align:center"><!-- Start Form Code -->
                        
                        <form name="myForm" action="http://smarttouchcrm.com/smarttouch-form.cfm" method="post" onSubmit="MM_validateForm('fname','','R','lname','','R','email','','RisEmail');return document.MM_returnValue">
                          <input type="hidden" id="aid" name="aid" value="100827093515030273396" />
                          <input type="hidden" id="fid" name="fid" value="110308171027780713184" />
                          <input type="hidden" id="rid" name="rid" value="" />
                          <input type="hidden" id="cid" name="cid" value="" />
                          <table border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                              <td align="right" valign="top"><table cellspacing="0" cellpadding="2" border="0">
                                  <tr>
                                    <td align="right" valign="top" style="text-align:right">First Name:</td>
                                    <td align="left"><input name="fname" type="text" id="fname" style="width: 120px;" value="" /></td>
                                  </tr>
                                  <tr>
                                    <td align="right" valign="top" style="text-align:right">Last Name:</td>
                                    <td align="left"><input name="lname" type="text" id="lname" style="width: 120px;" value="" /></td>
                                  </tr>
                                  <tr>
                                    <td align="right" valign="top" style="text-align:right">Email Address:</td>
                                    <td align="left"><input name="email" type="text" id="email" style="width: 120px;" value="" /></td>
                                  </tr>
                                  <tr> 
                                    <!-- Hardcode RRD Form as value 1 -->
                                    <input type="hidden" name="lsource" value="1">
                                </table></td>
                            </tr>
                          </table>
                          <script language="JavaScript" type="text/javascript">

<!--// 

function getVar(variable) { 

  var query = window.location.search.substring(1); 

  var vars = query.split("&"); 

  for (var i=0;i<vars.length;i++) { 

    var pair = vars[i].split("="); 

    if (pair[0] == variable) { 

      return pair[1]; 

	}

  } 

}

document.getElementById('rid').value=getVar('rid');

document.getElementById('cid').value=getVar('cid');

//-->

</script> 
                          
                          <!-- End Form Code -->
                          <table width="220" border="0" align="center" class="smallbodytext2">
                            <tr>
                              <td align="center"> By registering you are also opting in to receive electronic communications such as newsletters, account information, promotional emails and updates. We do not share your email with third parties; all emails will come from car2go.</td>
                            </tr>
                          </table>
                          <table border="0" cellpadding="0" cellspacing="7" width="300" align="center">
                            <tr>
                              <td align="center" style="text-align:center"><input type="submit" class="submit" value=""></td>
                            </tr>
                          </table>
                        </form>
                        <p align="left"><b><font color="#000000">REQUIRED: To complete your registration you must enter your driver's license and method of payment after you click the link.</font></b></td>
                    </tr>
                  </table>
                  <br>
                  A $2 plus tax annual fee will apply and is donated to <a href="http://www.canuckplace.org" target="_blank">Canuck Place Children's Hospice.</a></div>
              </div></td>
          </tr>
        </table></td>
      <td>&nbsp;</td>
    </tr>
    <tr valign="top" bgcolor="#E3F5FC">
      <td></td>
      <td class="hometext"><p><b>You have reached this page in error.</b>
        
        <p> Please click on one of the above buttons to navigate back to the main site.</p>
        <p></td>
      <td></td>
    </tr>
    <tr valign="top" bgcolor="#E3F5FC">
      <td></td>
      <td align="center" class="padbot"><table width="908" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td valign="top" width="440"><div class="topinset">
                <div class="bottominset" style="height:300px;">
                  <h3>The car2go concept &#8212; it  works!<br>
                    <br>
                  </h3>
                  <center>
                  <object width="370" height="240"><param name="movie" value="http://www.youtube.com/v/gjLbUslmxQc?hl=en&fs=1"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/gjLbUslmxQc?hl=en&fs=1" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="370" height="240"></embed></object>
                  
                  </center>
                </div>
              </div></td>
            <td width="20"></td>
            <td valign="top" width="440"><div class="topinset">
                <div class="bottominset"> 
                  <!-- TWITTER FEED -->
                  <div id="twit"> 
                    <script src="http://widgets.twimg.com/j/2/widget.js" type="text/javascript"></script> 
                    <script type="text/javascript">
new TWTR.Widget({
  version: 2,
  type: 'profile',
  rpp: 8,
  interval: 6000,
  width: 400,
  height: 300,
  theme: {
    shell: {
      background: '#eceff5',
      color: '#0652b0'
    },
    tweets: {
      background: '#009de0',
      color: '#ffffff',
      links: '#e1e1e1'
    }
  },
  features: {
    scrollbar: true,
    loop: false,
    live: true,
    hashtags: true,
    timestamp: false,
    avatars: false,
    behavior: 'all'
  }
}).render().setUser('car2goAustin').start();
</script> 
                  </div>
                </div>
              </div></td>
          </tr>
        </table></td>
      <td></td>
    </tr>
    <tr valign="top" bgcolor="#ffffff">
      <td></td>
      <td align="center" class="padtop"><table style="display: inline-table;" bgcolor="#009bda" border="0" cellpadding="0" cellspacing="0" width="908">
          <tr bgcolor="#FFFFFF">
            <td valign="top"><a href="index.php"><img name="car2gologo_s1" src="imgs/car2go-logo_s1.jpg" width="136" height="95" border="0" id="car2gologo_s1" alt="" /></a><br /></td>
            <td valign="top"><a href="simple.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('simplenav_1','','imgs/simple-nav_s2.jpg',1);"><img name="simplenav_1" src="imgs/simple-nav_s1.jpg" width="142" height="95" border="0" id="simplenav_1" alt="" /></a></td>
            <td valign="top"><a href="convenient.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('convenientnav_1','','imgs/convenient-nav_s2.jpg',1);"><img name="convenientnav_1" src="imgs/convenient-nav_s1.jpg" width="145" height="95" border="0" id="convenientnav_1" alt="" /></a></td>
            <td valign="top"><a href="affordable.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('affordablenav_1','','imgs/affordable-nav_s2.jpg',1);"><img name="affordablenav_1" src="imgs/affordable-nav_s1.jpg" width="145" height="95" border="0" id="affordablenav_1" alt="" /></a></td>
            <td valign="top"><a href="sustainable.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('sustainablenav_1','','imgs/sustainable-nav_s2.jpg',1);"><img name="sustainablenav_1" src="imgs/sustainable-nav_s1.jpg" width="145" height="95" border="0" id="sustainablenav_1" alt="" /></a></td>
            <td valign="top" align="right" bgcolor="#FFFFFF" width="195"></td>
          </tr>
        </table></td>
      <td></td>
    </tr>
    <tr valign="top">
      <td width="21" height="21"><img src="imgs/corner_left_bottom.png" width="21" height="21" alt="Portland Car Sharing"></td>
      <td bgcolor="#E3F5FC"></td>
      <td width="21" height="21"><img src="imgs/corner_right_bottom.png" width="21" height="21" alt="Portland Car Sharing"></td>
    </tr>
  </table>
</div>
<?php require_once('footer.php'); ?>