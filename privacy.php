<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>car2go Miami | Miami Car Sharing | Rideshare In Miami</title>
<link rel="Stylesheet" type="text/css" href="style.css" title="Menu Styles">
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<meta http-equiv="PRAGMA" content="NO-CACHE">
<meta http-equiv="Expires" content="0">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="imagetoolbar" content="no">
<meta name="description" content="car2go">
<meta name="keywords" content="car2go, smart, Miami">
<link rel="shortcut icon" href="http://www.car2go.com/favicon.ico" type="image/x-icon">
<link rel="icon" href="http://www.car2go.com/favicon.ico" type="image/x-icon">
<script src="SpryAssets/SpryEffects.js" type="text/javascript"></script>
<script type="text/javascript">
<!--
function MM_validateForm() { //v4.0
  if (document.getElementById){
    var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
    for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=document.getElementById(args[i]);
      if (val) { nm=val.name; if ((val=val.value)!="") {
        if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
          if (p<1 || p==(val.length-1)) errors+='- '+nm+' must contain an e-mail address.\n';
        } else if (test!='R') { num = parseFloat(val);
          if (isNaN(val)) errors+='- '+nm+' must contain a number.\n';
          if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
            min=test.substring(8,p); max=test.substring(p+1);
            if (num<min || max<num) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
      } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' is required.\n'; }
    } if (errors) alert('The following error(s) occurred:\n'+errors);
    document.MM_returnValue = (errors == '');
} }
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
function MM_effectAppearFade(targetElement, duration, from, to, toggle)
{
	Spry.Effect.DoFade(targetElement, {duration: duration, from: from, to: to, toggle: toggle});
}


<!--
var swf ='<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0" width="321" height="275"><param name="movie" value="car2go-HowItWorks2.swf"><param name="quality" value="high"><embed src="car2go-HowItWorks2.swf" width="321" height="275" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash"></embed></object>';

function flashSwap() 
{
	document.getElementById("movie").innerHTML = swf;
}
//-->
</script>
<style type="text/css" media="screen">
#flashContent {
	width:100%;
	height:100%;
}
</style>
		</head>
<body id="index">
<div align="center">
<table border="0" cellpadding="5" cellspacing="0" width="950">
    <tr>
      <td width="675">&nbsp;</td>
      <td align="right" valign="bottom"><iframe src="http://www.facebook.com/plugins/like.php?app_id=191729397539808&amp;href=http%3A%2F%2Fwww.facebook.com%2Fcar2go.austin&amp;send=false&amp;layout=button_count&amp;width=100&amp;show_faces=true&amp;action=like&amp;colorscheme=light&amp;font=arial&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:100px; height:21px;" allowTransparency="true"></iframe>
        
        <!-- AddThis Button BEGIN -->
        
    <?php require_once('add_this.php'); ?>
        <!-- AddThis Button END --></td>
    </tr>
  </table>
  <table border="0" cellpadding="0" cellspacing="0" width="950">
    <tr>
      <td width="21" height="21"><img src="imgs/corner_left_top.png" width="21" height="21" alt="Miami Car Sharing"></td>
      <td bgcolor="#ffffff" width="908"></td>
      <td width="21" height="21"><img src="imgs/corner_right_top.png" width="21" height="21" alt="Miami Car Sharing"></td>
    </tr>
    
    <tr>
      <td width="21" bgcolor="#FFFFFF"></td>
      <td bgcolor="#ffffff" width="908" style="padding-bottom:0px;"><table style="display: inline-table;" bgcolor="#009bda" border="0" cellpadding="0" cellspacing="0" width="908">
          <tr bgcolor="#FFFFFF">
            <td valign="top"><a href="index.php"><img name="car2gologo_s1" src="imgs/car2go-logo_s1.jpg" width="136" height="95" border="0" id="car2gologo_s1" alt="" /></a><br /></td>
            <td valign="top"><a href="simple.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('simplenav_s1','','imgs/simple-nav_s2.jpg',1);"><img name="simplenav_s1" src="imgs/simple-nav_s1.jpg" width="142" height="95" border="0" id="simplenav_s1" alt="" /></a></td>
            <td valign="top"><a href="affordable.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('affordablenav_s1','','imgs/affordable-nav_s2.jpg',1);"><img name="affordablenav_s1" src="imgs/affordable-nav_s1.jpg" width="145" height="95" border="0" id="affordablenav_s1" alt="" /></a></td>
            <td valign="top"><a href="sustainable.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('sustainablenav_s1','','imgs/sustainable-nav_s2.jpg',1);"><img name="sustainablenav_s1" src="imgs/sustainable-nav_s1.jpg" width="145" height="95" border="0" id="sustainablenav_s1" alt="" /></a></td>
            <td valign="top"><a href="howto.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('howtonav_s1','','imgs/howto-nav_s2.jpg',1);"><img name="howtonav_s1" src="imgs/howto-nav_s1.jpg" width="145" height="95" border="0" id="howtonav_s1" alt="" /></a></td>
            <td valign="top" align="right" bgcolor="#FFFFFF">&nbsp;</td>
          </tr>
        </table>
      
      </td>
      <td width="21" bgcolor="#FFFFFF"></td>
    </tr>
    
    <tr valign="top">
              <td width="950" colspan=3 valign="top" bgcolor="#ffffff"><div class="padtop padbot">
          <table cellpadding="0" cellspacing="0" border="0" width="950">
            <tr>
              <td width="21" bgcolor="#FFFFFF"></td>
              <td valign="top" style="padding-right:25px;"><a href="member.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('signuplink_s1','','imgs/signup-link_s2.jpg',1);"><img name="signuplink_s1" id="signuplink_s1" src="imgs/signup-link_s1.jpg" width="440" height="60" alt="Sign Up Now" border="0"></a></td>
              <td valign="top"><a href="drive.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('memberinfolink_s1','','imgs/memberinfo-link_s2.jpg',1);"><img name="memberinfolink_s1" id="memberinfolink_s1" src="imgs/memberinfo-link_s1.jpg" width="440" height="60" alt="Want to Learn More?" border="0"></a></td>
              <td width="21" bgcolor="#FFFFFF"></td>
            </tr>
          </table>
        </div></td>
            </tr>
    <tr bgcolor="#E3F5FC">
      <td></td>
      <td class="padtop"><h3>Privacy Policy</h3><p><b>Overview</b><br>
car2go recognizes the importance of protecting your personal information. We take the protection of your personal information seriously and we want each applicant and Member to feel comfortable when submitting applications for membership. Any personal information collected by car2go will be treated with care and consideration to existing laws related to protecting your privacy. This Privacy Policy provides information on how we collect, store, use and disclose any personal information provided to us in connection with the car2go membership application process. The protection of your privacy when processing your personal information is an important matter for us, which we account for in our business processes. This Privacy Policy does not apply to data from which personal identifying information has been removed. We retain the right to use such data in any way we deem necessary to advance our business interests. This Privacy Policy does not apply to other internet sites even if accessed via our website.</p>
      <p>For Canadian-resident Members only: <em>The Personal Information Protection and Electronic Documents Act</em> (PIPEDA) is a federal law in Canada that applies to car2go Canada Ltd.'s Canadian operations. PIPEDA regulates the collection, use and handling of personal information. In some provinces, provincial privacy laws also apply.</p>
        <p><b>Consent</b><br>
  When you provide us with your personal information, you consent and agree to our use and disclosure of your personal information in accordance with this Privacy Policy.</p>
  	
    <p>The personal information that we collect about you will be with your express or implied consent, by your application for membership. We may collect personal information directly from you by phone, fax, or e-mail; through your use of our website; or through your use of our vehicles.</p>
        
<p>You may choose not to provide some or all of your personal information to us, but this may prevent us from providing our services to you or limit our ability to provide you with the level of service that you would otherwise expect from us as a Member.</p>

        <p><b>Personal Information Generally Defined</b><br>
          "Personal information" means an individual's name in combination with the individual's address; date of birth; e-mail address; gender; driver's license number; government issued identification number; driver's abstract; account number; or credit or debit card number.</p>
          
      <p> "Personal information" also means any information from which a living individual can be identified directly, or indirectly, in particular by reference to an identification number or to one or more factors specific to their physical, physiological, mental, economic, cultural or social identity.</p>
      
     <p> Personal information does not include the name, title, businesses address or office telephone number of an employee of an organization, or the name, address, telephone number or such similar information of a person that is publicly available (e.g., such as the information that is available in a public telephone directory).</p>
     
     <p>For purposes of compliance with statutory requirements imposed on flow of personal data by the European Data Protection Directive, car2go fully incorporates by reference The Daimler Code of Conduct for Customers/Suppliers, including definitions contained therein, available at www. car2go.com.</p>
       
       
        <p><b>Use of Personal Information</b><br>
          As part of the membership application process, car2go may collect personal information, as defined in this Privacy Policy, for the purpose of evaluating applications for car2go car-sharing membership. Any information collected may also be stored, retained or used in other car2go business processes including, but not limited to, membership renewal, periodic membership validation, member account-management and payment authorization. Some of the personal information provided must necessarily be shared with third parties including, but not limited to, insurance companies, entities responsible for vehicle maintenance and vehicle safety call-centers. By initiating the application process, you consent to the sharing of your personal information for these purposes. Any information shared with a third party will not be beyond what is necessary and each third party will maintain its own Privacy Policy and enter into an appropriate data protection agreement with car2go. Any personal information collected but not stored or maintained in the regular course of business will be securely destroyed. </p>
          
        <p>TIn order to better serve you and to create an EcoScore of your driving habits, car2go may employ electronic trip data collection, including electronic and real-time connections to the vehicles internal systems, as well as external electronic hardware and software. This may provide functions such as vehicle location, speed, travel times and other personal information used to calculate and/or verify invoices, maintenance and safety. This data may also be used to prevent fraudulent usage of car2go vehicles and/or fines for late returns and other use outside the boundaries of the vehicle reservation.</p>
        
        <p><b>Collection of Personal Information</b><br>
          Through the car2go website visitors can gather information about car-sharing and car2go membership. Personal information directly requested by and submitted in connection with the membership application process is collected and stored only if you voluntarily submit it to car2go. Other information about you is automatically collected. This automatically collected information includes your IP address, hosting server, browser, operating system, browser language and service provider.</p>
          
          <p>The servers used by car2go are located within Germany and the United States. For Canadian Members: Notwithstanding the location of the servers in Germany and the United States, car2go Canada Ltd. will ensure your personal information is protected in accordance with applicable Canadian privacy laws. Note, however, that such information is subject to the laws of Germany and the United States.</p>
          
          <p>car2go may also record telephone conversations with Members in connection with memberships, in order to clear up legal issues and/or to improve the quality of its vehicles and services.</p>
          
          <p>As part of the normal course of your membership and use of the service, car2go may also have access to, and collect, personal information about you from: traffic tickets, electronic highway toll systems, parking lot tickets and data systems. Where this information pertains to monitoring the safe, lawful and agreed-upon use of our vehicles including, but not limited to whether you were in control of a vehicle at a specific time as can be ascertained by such personal information, you give car2go your express consent to obtain and use this personal information.</p>
          
          
        <p><b>Sharing of Personal Information with Third Parties</b><br>
          car2go may share personal information with third parties acting on its behalf or at its direction for purposes relating to car2go operation, maintenance, administration, improvement and Member oversight. </p>
          
        <p>Disclosure of personal information may occur in connection with a corporate transaction, proceeding or reorganization of car2go business processes for which the information is maintained. Personal information may also be disclosed as permitted or required by law or when car2go believes disclosure is necessary to protect its rights and/or comply with a judicial proceeding or court order. car2go does not disclose or sell your personal information to third parties for marketing purposes.</p>
        <p>Without limiting the foregoing, we may disclose your personal information to:</p>
          <ul>
          <li>your company or organization if you use our services under a corporate account;</li>
          <li>your credit card issuers;</li>
          <li>credit reporting and fraud checking agencies;</li>
          <li>debt collection agencies, if you fail to pay monies owed to us;</li>
          <li>government or private organizations responsible for the processing or handling of traffic or parking related violations;</li>
          <li>driver licensing authorities, directly or through intermediary organizations, when needed to verify compliance with car2go's safe driver criteria and/or to verify license personal information;</li>
          <li>government, regulatory and law enforcement agencies where the disclosure is required or authorized by law; and</li>
          <li>insurance companies.</li>
          </ul>
          
          
          
          
        <p><b>Security and Notice of Breach</b><br>
          car2go complies with industry standards to protect the personal information it maintains from misuse, loss, unauthorized access and modification or disclosure in violation of this Privacy Policy. This protection applies in relation to personal information stored in both electronic and hard copy form. car2go will notify its Members as promptly as possible of any privacy breach after discovering or receiving notification of the breach if car2go reasonably believes the information has been acquired by an unauthorized person or subject to an unauthorized use.</p>
          <p>Access to the car2go databases are restricted to authorised personnel and is password protected. Information transmitted through our website is encrypted.</p>
          
        <p><b>Access to Personal Information</b><br>
          Upon request, you may access any of your personal information collected by car2go, that car2go maintains at the time of your request. car2go will provide reasonable and practical access to your personal information. We will normally provide access without charge unless you either request access to a large volume of personal information or we have to access archived records to obtain the personal information. In these circumstances, we may impose a reasonable fee. We will, however advise you of that fee in advance. You may challenge the reasonableness of the cost. Details of how to contact us are set out below. There may be instances where we may not be able to provide you access to your personal information, for certain reasons. Among these are; it has been destroyed or deleted after expiry of applicable retention periods, it contains personal information of other persons or it contains commercially sensitive or proprietary information owned by us. If we are unable to provide you access, we will explain why and document that for our records.</p>
          
        <p><b>Accuracy/Correction of Personal Information</b><br>
          When reasonable and commercially practicable, car2go upon notice, will correct any inaccuracies in your personal information. Personal information may also be accessed and edited personally at www.car2go.com/toronto or www.car2gotoronto.com.</p>
          
          <p><b>Changes to Privacy Policy</b><br />
          car2go reserves the right to change this Privacy Policy from time to time. Any changes to this Privacy Policy will be posted at www.car2go.com/vancouver or www.car2govancouver.com. All changes are effective ten days after posting.</p>
          
          <p><b>Questions/Complaints</b><br />
Questions and complaints regarding car2go handling of personal information should be directed to vancouver@car2go.com or car2go c/o William Knapp, 800 W 5th St # 100B Austin, TX 78703-5434, United States.</p>

          
        <p><b>Use of Website Tracking:</b><br />
Use of Internet Protocol (IP) Addresses: An IP address is a unique number that is automatically assigned to your computer whenever you are surfing the Internet so that your computer can be identified by the main computers, known as Web servers that serve Web pages. This allows us to identify and gather general information about your use of the site, such as the Web pages viewed on car2go to provide you service.</p>

<p>car2go collects IP addresses for the purposes of: helping us diagnose problems with our main computers, for system administration, to report aggregated personal information to our business partners and to audit the use of our website. When users request web pages from our website, our web servers log the User's IP address. We do not normally link IP addresses to anythingpersonally identifiable, which means that a user's session will be logged, but the user will remain anonymous to us. For example, we collect and/or track the home server domain name, the type of computer, and the type of web browser used by you to access our website. Such collection and tracking personal information is gathered by us as you navigate through our website, and will be used by us for our business purposes only. We can, and will, use IP addresses to identify you when we feel it is necessary to enforce compliance with our website Terms of Use or to protect our service, site, users, or others.</p>


<div>
  <div> </div>
</div>
<p>This Privacy Policy is effective April 26, 2012.</p></td>
      <td></td>
    </tr>
    <tr valign="top">
      <td width="21" height="21"><img src="imgs/corner_left_bottom.png" width="21" height="21" alt="Miami Car Sharing"></td>
      <td bgcolor="#E3F5FC"></td>
      <td width="21" height="21"><img src="imgs/corner_right_bottom.png" width="21" height="21" alt="Miami Car Sharing"></td>
    </tr>
  </table>
</div>
<?php require_once('footer.php'); ?>