<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>car2go Miami | Car Rentals | Miami car2go car share</title>
<meta name="description" content=" Follow these easy steps to rent an Miami car2go. Get a rental car by simply following the car rental steps. "/>
<meta id="MetaKeywords" name="KEYWORDS" content=" car sharing, car2go Miami, get a car, car share, car to go, car rentals" />
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<meta http-equiv="PRAGMA" content="NO-CACHE">
<meta http-equiv="Expires" content="0">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="imagetoolbar" content="no">
<link rel="shortcut icon" href="http://www.car2go.com/favicon.ico" type="image/x-icon">
<link rel="Stylesheet" type="text/css" href="style.css" title="Menu Styles">
<link rel="icon" href="http://www.car2go.com/favicon.ico" type="image/x-icon">
<script src="gen_validatorv4.js" type="text/javascript"></script>

<script src="SpryAssets/SpryEffects.js" type="text/javascript"></script>
<script type="text/javascript">

<!--
function MM_validateForm() { //v4.0
  if (document.getElementById){
    var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
    for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=document.getElementById(args[i]);
      if (val) { nm=val.name; if ((val=val.value)!="") {
        if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
          if (p<1 || p==(val.length-1)) errors+='- '+nm+' must contain an e-mail address.\n';
        } else if (test!='R') { num = parseFloat(val);
          if (isNaN(val)) errors+='- '+nm+' must contain a number.\n';
          if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
            min=test.substring(8,p); max=test.substring(p+1);
            if (num<min || max<num) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
      } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' is required.\n'; }
    } if (errors) alert('The following error(s) occurred:\n'+errors);
    document.MM_returnValue = (errors == '');
} }
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
function MM_effectAppearFade(targetElement, duration, from, to, toggle)
{
	Spry.Effect.DoFade(targetElement, {duration: duration, from: from, to: to, toggle: toggle});
}


<!--
var swf ='<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0" width="321" height="275"><param name="movie" value="car2go-HowItWorks2.swf"><param name="quality" value="high"><embed src="car2go-HowItWorks2.swf" width="321" height="275" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash"></embed></object>';

function flashSwap() 
{
	document.getElementById("movie").innerHTML = swf;
}
//-->
</script>
<style type="text/css" media="screen">
#flashContent {
	width:100%;
	height:100%;
}
</style>

</head>
<body id="index">
<div align="center">
  <table border="0" cellpadding="5" cellspacing="0" width="950">
    <tr>
      <td width="675">&nbsp;</td>
      <td align="right" valign="bottom"><iframe src="http://www.facebook.com/plugins/like.php?app_id=191729397539808&amp;href=http%3A%2F%2Fwww.facebook.com%2Fcar2go.austin&amp;send=false&amp;layout=button_count&amp;width=100&amp;show_faces=true&amp;action=like&amp;colorscheme=light&amp;font=arial&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:100px; height:21px;" allowTransparency="true"></iframe>
        
        <!-- AddThis Button BEGIN -->
        
    <?php require_once('add_this.php'); ?>
        <!-- AddThis Button END --></td>
    </tr>
  </table>
  <table border="0" cellpadding="0" cellspacing="0" width="950">
    <tr>
      <td width="21" height="21"><img src="imgs/corner_left_top.png" width="21" height="21" alt="Miami Car Sharing"></td>
      <td bgcolor="#ffffff" width="908"></td>
      <td width="21" height="21"><img src="imgs/corner_right_top.png" width="21" height="21" alt="Miami Car Sharing"></td>
    </tr>
    
    <tr>
      <td width="21" bgcolor="#FFFFFF"></td>
      <td bgcolor="#ffffff" width="908" style="padding-bottom:20px;"><table style="display: inline-table;" bgcolor="#009bda" border="0" cellpadding="0" cellspacing="0" width="908">
          <tr bgcolor="#FFFFFF">
            <td valign="top"><a href="index.php"><img name="car2gologo_s1" src="imgs/car2go-logo_s1.jpg" width="136" height="95" border="0" id="car2gologo_s1" alt="" /></a><br /></td>
            <td valign="top"><a href="simple.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('simplenav_s1','','imgs/simple-nav_s2.jpg',1);"><img name="simplenav_s1" src="imgs/simple-nav_s1.jpg" width="142" height="95" border="0" id="simplenav_s1" alt="" /></a></td>
            <td valign="top"><a href="affordable.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('affordablenav_s1','','imgs/affordable-nav_s2.jpg',1);"><img name="affordablenav_s1" src="imgs/affordable-nav_s1.jpg" width="145" height="95" border="0" id="affordablenav_s1" alt="" /></a></td>
            <td valign="top"><a href="sustainable.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('sustainablenav_s1','','imgs/sustainable-nav_s2.jpg',1);"><img name="sustainablenav_s1" src="imgs/sustainable-nav_s1.jpg" width="145" height="95" border="0" id="sustainablenav_s1" alt="" /></a></td>
            <td valign="top"><a href="howto.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('howtonav_s1','','imgs/howto-nav_s2.jpg',1);"><img name="howtonav_s1" src="imgs/howto-nav_s1.jpg" width="145" height="95" border="0" id="howtonav_s1" alt="" /></a></td>
            <td valign="top" align="right" bgcolor="#FFFFFF">&nbsp;</td>
          </tr>
        </table>
      </td>
      <td width="21" bgcolor="#FFFFFF"></td>
    </tr>
    
    <tr valign="middle" bgcolor="#ffffff">
      <td>&nbsp;</td>
      <td valign="top">
      
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="21" height="21"><img src="imgs/blue_corner_topleft.gif" width="22" height="22" alt="Miami Car Share"></td>
    <td height="21" bgcolor="#EBF5FA" width="100%">&nbsp;</td>
    <td width="21" height="21"><img src="imgs/blue_corner_topright.gif" width="22" height="22" alt="Miami Car Share"></td>
  </tr>
</table>
      
      </td>
      <td>&nbsp;</td>
    </tr>
    
    
    <tr valign="middle" bgcolor="#ffffff">
      <td>&nbsp;</td>
      <td align="center" valign="top" bgcolor="#EBF5FA"><div class="leftside-txt"><img src="imgs/member-head.jpg" width="226" height="79" alt="Becoming a Member">
      </div>
        <div class="rightside-txt">
          <p>Aside from having more elbow room and cost savings, and being better for the environment, car2go is a fun transportation alternative complementing your lifestyle &mdash; whatever your mobility needs.<br><br>
        Registration is simple, fast and easy! In just a few minutes you become a part of the car2go mobility movement. You&rsquo;ll need your driver&rsquo;s license and credit card information. There is a one-time sign up fee of $35, which will be charged only when your membership is approved* by car2go.  If you have a valid promotion code, you can enter it during registration. Once registered and approved as a car2go member, you will receive a member card that allows you to unlock and drive a car2go.<br/><br/>
*Please allow 3-5 days for approval.</p></div></td>
      <td>&nbsp;</td>
    </tr>
    
    <tr valign="middle" bgcolor="#ffffff">
      <td>&nbsp;</td>
      <td valign="top">
      
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="21" height="21"><img src="imgs/blue_corner_bottomleft.gif" width="22" height="22" alt="Miami Car Share"></td>
    <td height="21" bgcolor="#EBF5FA" width="100%">&nbsp;</td>
    <td width="21" height="21"><img src="imgs/blue_corner_bottomright.gif" width="22" height="22" alt="Miami Car Share"></td>
  </tr>
</table>
      
      </td>
      <td>&nbsp;</td>
    </tr>
    
    <tr valign="top">
      <td width="950" colspan=3 valign="top" bgcolor="#ffffff"><div class="padtop padbot">
          <table cellpadding="0" cellspacing="0" border="0" width="950">
            <tr>
              <td width="21" bgcolor="#FFFFFF"></td>
              <td valign="top" style="padding-right:25px;"><a href="member.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('signuplink_s1','','imgs/signup-link_s2.jpg',1);"><img name="signuplink_s1" id="signuplink_s1" src="imgs/signup-link_s1.jpg" width="440" height="60" alt="Sign Up Now" border="0"></a></td>
              <td valign="top"><a href="drive.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('memberinfolink_s1','','imgs/memberinfo-link_s2.jpg',1);"><img name="memberinfolink_s1" id="memberinfolink_s1" src="imgs/memberinfo-link_s1.jpg" width="440" height="60" alt="Want to Learn More?" border="0"></a></td>
              <td width="21" bgcolor="#FFFFFF"></td>
            </tr>
          </table>
        </div></td>
    </tr>
    <tr valign="middle" bgcolor="#E3F5FC">
      <td>&nbsp;</td>
      <td class="padtop"><table border="0" cellpadding="0" cellspacing="0" width="908">
          <tr>
            <td valign="top" width="440"><div class="topinset">
                <?php require_once('order.php'); ?>
              </div></td>
            <td width="20"></td>
            <td valign="bottom" align="right" width="440" style="text-align:right;"><img src="imgs/car.jpg" alt="car2go" width="211" height="164"><br>
              <a href="/" class="backhome"><b>< Back to Home</b></a></td>
          </tr>
        </table></td>
      <td>&nbsp;</td>
    </tr>
    <tr valign="top">
      <td width="21" height="21"><img src="imgs/corner_left_bottom.png" width="21" height="21" alt="Miami Car Sharing"></td>
      <td bgcolor="#E3F5FC"></td>
      <td width="21" height="21"><img src="imgs/corner_right_bottom.png" width="21" height="21" alt="Miami Car Sharing"></td>
    </tr>
  </table>
</div>
<?php require_once('footer.php'); ?>