<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>car2go Toronto | Toronto Car Sharing | Rideshare In Toronto</title>
<link rel="Stylesheet" type="text/css" href="style.css" title="Menu Styles">
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<meta http-equiv="PRAGMA" content="NO-CACHE">
<meta http-equiv="Expires" content="0">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="imagetoolbar" content="no">
<meta name="description" content="car2go">
<link rel="shortcut icon" href="http://www.car2go.com/favicon.ico" type="image/x-icon">
<link rel="icon" href="http://www.car2go.com/favicon.ico" type="image/x-icon">
</style>
<script src="SpryAssets/SpryEffects.js" type="text/javascript"></script>
<script type="text/javascript">
<!--
function MM_validateForm() { //v4.0
  if (document.getElementById){
    var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
    for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=document.getElementById(args[i]);
      if (val) { nm=val.name; if ((val=val.value)!="") {
        if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
          if (p<1 || p==(val.length-1)) errors+='- '+nm+' must contain an e-mail address.\n';
        } else if (test!='R') { num = parseFloat(val);
          if (isNaN(val)) errors+='- '+nm+' must contain a number.\n';
          if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
            min=test.substring(8,p); max=test.substring(p+1);
            if (num<min || max<num) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
      } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' is required.\n'; }
    } if (errors) alert('The following error(s) occurred:\n'+errors);
    document.MM_returnValue = (errors == '');
} }
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
function MM_effectAppearFade(targetElement, duration, from, to, toggle)
{
	Spry.Effect.DoFade(targetElement, {duration: duration, from: from, to: to, toggle: toggle});
}


<!--
var swf ='<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0" width="321" height="275"><param name="movie" value="car2go-HowItWorks2.swf"><param name="quality" value="high"><embed src="car2go-HowItWorks2.swf" width="321" height="275" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash"></embed></object>';

function flashSwap() 
{
	document.getElementById("movie").innerHTML = swf;
}
//-->
</script>
<style type="text/css" media="screen">
#flashContent {
	width:100%;
	height:100%;
}
</style>
		</head>
<body id="index">
<div align="center">
<table border="0" cellpadding="5" cellspacing="0" width="950">
    <tr>
      <td width="675">&nbsp;</td>
      <td align="right" valign="bottom"><iframe src="http://www.facebook.com/plugins/like.php?app_id=191729397539808&amp;href=http%3A%2F%2Fwww.facebook.com%2Fcar2go.austin&amp;send=false&amp;layout=button_count&amp;width=100&amp;show_faces=true&amp;action=like&amp;colorscheme=light&amp;font=arial&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:100px; height:21px;" allowTransparency="true"></iframe>
        
        <!-- AddThis Button BEGIN -->
        
    <?php require_once('add_this.php'); ?>
        <!-- AddThis Button END --></td>
    </tr>
  </table>
  <table border="0" cellpadding="0" cellspacing="0" width="950">
    <tr>
      <td width="21" height="21"><img src="imgs/corner_left_top.png" width="21" height="21" alt="Toronto Car Sharing"></td>
      <td bgcolor="#ffffff" width="908"></td>
      <td width="21" height="21"><img src="imgs/corner_right_top.png" width="21" height="21" alt="Toronto Car Sharing"></td>
    </tr>
    <tr>
      <td width="21" bgcolor="#FFFFFF"></td>
      <td bgcolor="#ffffff" width="908"><table bgcolor="#009bda" border="0" cellpadding="0" cellspacing="0" width="908">
          <tr bgcolor="#FFFFFF">
            <td valign="top"><a href="/"><img name="car2gologo_s1" src="imgs/car2go-logo_s1.jpg" width="136" height="95" border="0" id="car2gologo_s1" alt="" /></a><br />
              </td>
            <td valign="top"><a href="simple.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('simplenav_s1','','imgs/simple-nav_s2.jpg',1);"><img name="simplenav_s1" src="imgs/simple-nav_s1.jpg" width="142" height="95" border="0" id="simplenav_s1" alt="" /></a></td>
            <td valign="top"><a href="convenient.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('convenientnav_s1','','imgs/convenient-nav_s2.jpg',1);"><img name="convenientnav_s1" src="imgs/convenient-nav_s1.jpg" width="145" height="95" border="0" id="convenientnav_s1" alt="" /></a></td>
            <td valign="top"><a href="affordable.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('affordablenav_s1','','imgs/affordable-nav_s2.jpg',1);"><img name="affordablenav_s1" src="imgs/affordable-nav_s1.jpg" width="145" height="95" border="0" id="affordablenav_s1" alt="" /></a></td>
            <td valign="top"><a href="sustainable.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('sustainablenav_s1','','imgs/sustainable-nav_s2.jpg',1);"><img name="sustainablenav_s1" src="imgs/sustainable-nav_s1.jpg" width="145" height="95" border="0" id="sustainablenav_s1" alt="" /></a></td>
            <td valign="top" align="right" bgcolor="#FFFFFF"><div id="flashContent">
                      <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="195" height="115" id="countdown" align="top">
                      <param name="movie" value="countdown.swf" />
                      <param name="quality" value="high" />
                      <param name="bgcolor" value="#ffffff" />
                      <param name="play" value="true" />
                      <param name="loop" value="true" />
                      <param name="wmode" value="transparent" />
                      <param name="scale" value="showall" />
                      <param name="menu" value="true" />
                      <param name="devicefont" value="false" />
                      <param name="salign" value="" />
                      <param name="allowScriptAccess" value="sameDomain" />
                      <!--[if !IE]>-->
                      <object type="application/x-shockwave-flash" data="countdown.swf" width="195" height="115">
                          <param name="movie" value="countdown.swf" />
                          <param name="quality" value="high" />
                          <param name="bgcolor" value="#ffffff" />
                          <param name="play" value="true" />
                          <param name="loop" value="true" />
                          <param name="wmode" value="transparent" />
                          <param name="scale" value="showall" />
                          <param name="menu" value="true" />
                          <param name="devicefont" value="false" />
                          <param name="salign" value="" />
                          <param name="allowScriptAccess" value="sameDomain" />
                          <!--<![endif]--> 
                          
                          <!--[if !IE]>-->
                        </object>
                      <!--<![endif]-->
                    </object>
                    </div></td>
          </tr>
        </table></td>
      <td width="21" bgcolor="#FFFFFF"></td>
    </tr>
    <tr valign="top">
      <td width="950" colspan=3 valign="top" bgcolor="#ffffff"><div class="padtop padbot">
          <table cellpadding="0" cellspacing="0" border="0" width="950">
            <tr>
              <td width="21" bgcolor="#FFFFFF"></td>
              <td valign="top"><a href="member.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('memberlink_s1','','imgs/member-link_s2.jpg',1);"><img name="memberlink_s1" src="imgs/member-link_s1.jpg" width="453" height="73" border="0" id="memberlink_s1" alt="" /></a></td>
              <td valign="top"><a href="drive.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('drivenav_s1','','imgs/drive-nav_s2.jpg',1);"><img name="drivenav_s1" src="imgs/drive-nav_s1.jpg" width="455" height="73" border="0" id="drivenav_s1" alt="" /></a></td>
              <td width="21" bgcolor="#FFFFFF"></td>
            </tr>
          </table>
        </div></td>
    </tr>
    <tr bgcolor="#E3F5FC">
      <td></td>
      <td class="padtop"><h3>Terms and Conditions </h3>
        <p>car2go Canada Ltd. (&ldquo;car2go&rdquo;) offers car-sharing memberships to private individuals and authorized legal entities that have been pre-approved through an application process and registered as car2go Members.  This Agreement is a contract and governs the relationships, rights, and obligations between car2go and the Member.  A car2go Member must agree to the terms and conditions contained in this Agreement (&ldquo;Terms and Conditions&rdquo;) before reserving and using a car2go vehicle located in Canada.</p>
        <p><b>Section 1 – Definitions</b></p>
        <blockquote>
          <p>1.1 <b>Business Area</b><br>
          The area predefined by car2go as the exclusive area for commencement and termination of a Rental Period. A map of the current Business Area is available and displayed at www.car2go.com.</p>
          
          <p>1.2 <b>Driver</b><br>
           An individual who is authorized to drive a vehicle under these Terms and Conditions as a
Member or an authorized user with drive privileges at the expense of a Member.</p>

			<p>1.3	<b>Fee Schedule</b><br>
			The then current fee schedule available at www.car2go.com.</p>

          <p>1.4 <b>Fixed Area of Operation</b><br>
          The predefined area identified by car2go at the time of rental and a 300 kilometre radius outside the predefined area but never including any area not within Canada.</p>
            
          <p>1.5 <b>Member</b><br>
           An individual approved through an application process and registered as an authorized
car2go user and who has agreed to these Terms and Conditions and the Rental Process.</p>
          <p>1.6 <b>Privacy Policy</b><br>
          car2go's privacy policy, available online at: www.car2go.com. </p>
          
            1.7 <b>Rental Period</b><br>
            The span of time beginning when a Member commences a trip pursuant to Section 5 below
and ending when the trip is ended pursuant to Section 6 below.</p>

          <p>1.8 <b>Schedules</b>  <br>
             All the schedules to this Agreement, including the currently effective fee schedule.</p>
      
      <p>1.9 <b>Terms and Conditions</b><br>
            These membership terms and conditions including its Schedules, whether made available in print or electronically through car2go's website, and periodic guidelines, rules or restrictions issued by car2go from time to time.
       
        <p>1.10	<b>Usage Statement</b><br>
         A summary of all fees assessed to a Member's account by car2go, generated on a regular basis, and made available at www.car2go.com.  </p>  
            
            
        </blockquote>
        <p><b>Section 2 – Terms of Use</b> </p>
        <blockquote>
          <p>2.1 These Terms and Conditions are a car-sharing membership contract but do not confer any right to use car2go vehicles. Members may use car2go vehicles, to the extent available, in accordance with these Terms and Conditions.</p>
          
          <p>2.2 The Member represents and warrants to car2go that it has received these Terms and Conditions, the associated Rental Process terms, and all reasonable explanations concerning the content of these documents, including all Schedules, and has carefully reviewed and accepted Member commitments and obligations thereunder.</p>
          
        </blockquote>
        <p><b>Section 3 – Drive Authorization</b></p>
        <blockquote>
          <p>3.1 The only persons who shall be authorized to drive car2go car-sharing vehicles are persons who:</p>
          <ol>
            <li>are Members or authorized Drivers as defined in Section 1;</li>
            <li>hold and carry during every trip a valid driver&rsquo;s licence issued in a United States or Canadian jurisdiction authorizing the holder to drive a passenger vehicle, and who meet all of the conditions, restrictions, or other requirements that may be contained therein; </li>
            <li>are at least 19 years of age, unless otherwise agreed to by contract.  </li>
            <li>have a valid car2go Membership card in accordance with Section 4;</li>
            <li>are of sound mind and memory and have not used any drugs, imbibed any alcohol or taken any medication that could adversely affect their ability to drive (for alcohol, a zero-tolerance policy is adopted and a limit of 0.0% applies); and </li>
            <li>have selected a method of payment in the car2go internet portal and have registered the corresponding data or Drivers who are authorized to drive at the expense of a Member.</li>
          </ol>
          <p>The suspension, revocation, expiry, surrender or loss of a Member's driver's licence shall result in the immediate revocation of drive authorization and Member and/or Driver status.</p>
          <p>3.2 All Drivers are strictly prohibited from permitting third parties unauthorized by car2go to drive vehicles. Unauthorized third parties include a third-party Member who is not the Driver of record for the Rental Period. For each violation of this prohibition, the Member shall pay liquidated damages in the amount of $1,500 per violation. car2go reserves the right to claim further damages.</p>
        </blockquote>
        
        <p><b>Section 4 – Means of Access</b></p>
        <blockquote>
          <p>4.1 Members will have a car2go RFID chip embedded into a membership card issued by car2go.  As a result, the Member is able to rent and access car2go vehicles.</p>
          <p>4.2 The car2go membership card and associated RFID chip remains the property of car2go.  Removing the chip from the membership card will destroy its electronic function.  Separating the chip in any way from the membership card is prohibited.  Any violations of this provision shall result in the immediate revocation of drive authorization and Member or Driver status. </p>
          <p>4.3 Any use of information technology in an attempt to read, copy or manipulate the RFID chip is prohibited.  Any violations of this provision shall result in the immediate revocation of drive authorization and Member and/or Driver status.   The Member shall bear the costs of any damage that may potentially result from such violation.</p>
          <p>4.4 Drivers agree to immediately report the loss of a membership card to car2go (via the internet portal or by telephone to the Service Centre), so that car2go can deactivate the RFID chip and prevent any improper use. The Member will be informed of such deactivation via e-mail to the e-mail address provided by the Member at the time of registration.</p>
          <p>4.5 Members shall pay a fee, as determined by car2go, for the replacement of lost or damaged RFID chips or membership cards assigned to the Member and/or the Member's authorized Drivers. The fee is provided in the Fee Schedule. Members shall be responsible for all damages resulting from loss of a membership card, including but not limited to, theft, damage or improper use of a vehicle.        </p>
        </blockquote>
        <p><b>Section 5 – Reservation and Commencement of Rental Period</b></p>
        <blockquote>
          <p>5.1 Members may reserve vehicles as early as 24 hours in advance and up to 15 minutes before commencing the rental. Reservations can be made online at the car2go website or by phone (additional reservation methods may be made available in the future), however, no more than two reservations may be made in a 24-hour period and all reservations must be separated by at least 30 minutes.</p>
          
          <p>5.2 car2go retains the right to refuse advance booking if the number of available vehicles is not sufficient to fulfill all reservation requests. If car2go accepts the advanced reservation, the Driver will be informed via an instant text message (SMS) of the location of the reserved car2go vehicle 15 minutes before the reservation time. Members should note that cell phone providers may charge a fee for receiving text messages. Members should contact their cell phone providers for any fees associated with receipt of text messages. car2go is not responsible for any fees assessed by the Member's or Driver's cell phone provider for receipt of text messages.</p>
          <p>5.3A vehicle may not be reserved or rented for more than five consecutive days. If a Rental Period exceeds five consecutive days, the Rental Period will be terminated by car2go and the Member will incur penalties and a manual per-day charge, as specified in the Fee Schedule, until the vehicle is returned to the Business Area. car2go further reserves the right to process payment for any used minutes after expiration of two days, even if the Rental Period has not been terminated. If payment is rejected or otherwise unauthorized, car2go retains the right to repossess the vehicle from its then current location and the Member and/or Driver will assume all associated costs.</p>
          <p>5.4 Reservations may be cancelled up to 15 minutes before the reservation time without charge. Any reservation cancelled less than 15 minutes before reservation time will result in a charge in accordance with the Fee Schedule.</p>
          <p>5.5 Members may also choose to locate an available vehicle in any area and commence a rental spontaneously, without a prior reservation. Such spontaneous use is possible only for those car2go vehicles that are marked &quot;available&quot; as evidenced by a green status light and whose card reader displays a corresponding &quot;available&quot; notice.</p>
          <p>5.6 Before operating a vehicle, Members must assess the interior and exterior of the vehicle for any visible defects, damages or excessive soiling, and must notify car2go of any observed defects, damages or problems to or with the vehicle itself or with any installed technology including car2go installed screens, pursuant to the car2go Rental Process. A telephone connection between the vehicle and the Service Centre will be established to ascertain the nature and seriousness of any reported defects, damages and/or soiling. The Member and/or Driver must make complete and truthful statements. The Service Centre is entitled to prohibit the Driver from using the car2go vehicle if the reported defects, damages or soiling is deemed a safety hazard. Failure to report any deficiency can result in the last renting Member being held responsible for the repair or cleaning of the vehicle.</p>
          <p>5.7 If the Member-reported observations result in the vehicle being deemed inoperable, the Member will be assigned a different vehicle within walking distance. If no vehicle is available, the Service Centre can arrange a cab pick-up at the Member's expense.</p>
          
         <p> 5.8	car2go will depend on its Members to return vehicles in a clean state. As a result, car2go cannot guarantee that a car2go vehicle will be clean and does not take responsibility for objects, if any, left in a vehicle by another Member or Driver.</p>
          
		<p>5.9	The Service Centre may contact the Member and/or Driver by telephone in the event of disruptions to the normal course of use (including, but not limited to, failure to commence a trip within a 30-minute period after a Member rents a vehicle; or if the vehicle is parked for 15 minutes or more without having been locked) in order to establish the cause of the disruption to the normal course of use. The incoming call is directed to the vehicle Driver's cell phone. The Service Centre may prohibit any continued use of the vehicle and/or notify the appropriate authorities if it has cause to suspect behaviour violating any laws or this Agreement.</p>
        
		<p>5.10	The Rental Period is commenced as soon as the Driver holds the RFID chip embedded in the membership card to the card reader installed in the windshield area, and the onboard computer has confirmed receipt of the information stored on the electronic key by unlocking the central locking system.</p>
        
		<p>5.11	The Rental Period ends upon the termination of the rental by the Member with due care, in accordance with Section 6 below.</p>
        
        <p>5.12	During the Rental Period, use of a vehicle outside the Fixed Area of Operation is prohibited. car2go support services may be limited when a Member and/or Driver travel outside the Fixed Area of Operation. At no time should a vehicle leave Canadian territory. If a vehicle is driven outside the Fixed Area of Operation, the Member and/or Driver will be personally responsible for costs associated with returning the vehicle to the Business Area, including, but not limited to, costs associated with vehicle repair, motor-vehicle accidents or collisions and towing services. The Member will continue to incur rental charges until the vehicle is returned to the Business Area. At its sole discretion, car2go reserves the right to repossess any car2go vehicle operating outside the Fixed Area of Operation at any time during or after the Rental Period.
          </p>
         
        </blockquote>
        <p><b>Section 6 – End of Rental Term </b></p>
        <blockquote>
          <p>6.1 When the Member and/or Driver wishes to end a rental, the Member and/or Driver must:</p>
          <ol type="a">
            <li>park the vehicle, at his/her choice, in a parking spot within an area designated and marked as a car2go car-sharing parking spot or in another parking space for passenger vehicles authorized by car2go. The Member will be personally responsible for any traffic, moving or parking violation citations issued for failure to comply with traffic rules or other laws and any highway or bridge toll charges levied against the vehicle during the Rental Period;</li>
            <li>ensure that the key, fuel card and, if applicable, parking card have been returned to holders in the car2go vehicle;</li>
            <li>To ensure that all windows and doors are fully closed and that all lights have been turned off, and</li>
            <li>To ensure that no trash or soiling remains in the  vehicle.</li>
          </ol>
          <p>6.2 The rental may be ended only if the vehicle is located within the Business Area. The exact boundaries are shown at all times on the website at www.car2go.com. A rental may only be commenced and ended within the Business Area.</p>
          <p>6.3 The rental may be ended only if, at the location at which the vehicle is parked, a connection with a cell phone can be established. If this is not possible, the Driver will have to park the car2go vehicle at another car2go authorized location.</p>
          <p>6.4 The end of the rental is initiated by the Driver holding the membership card with a valid RFID chip to the vehicle once again (card reader in the windshield area). The rental is ended only if the display unit of the card reader has correspondingly confirmed this. If the Driver leaves the vehicle before receiving confirmation that the Rental Period has ended, the Driver shall continue to accrue costs at his or her own expense or at the expense of the applicable Member.</p>
        </blockquote>
        <p><b>Section 7 – Settlement Account, Use Authorization Across Accounts</b></p>
        <blockquote>
          <p>7.1 To enable the rent and use of the vehicle, Members who are individuals must:</p>
          <ol type="a">
            <li>have either selected a payment method on the internet portal and must have provided the corresponding data; or</li>
            <li>have been authorized by a Member, with his or her own settlement account, to rent/use car2go vehicles at his or her expense.</li>
          </ol>
          <p>7.2 Members authorizing Drivers to charge their rental to the Member's settlement account shall be responsible for all fees and costs resulting from the Driver's use, including charges and fees in accordance with the Fee Schedule. Members shall also be responsible for any damages caused by the Driver, and car2go's loss of vehicle use in cases of accidents, vehicles being impounded, and improper use resulting in a vehicle being out of service. Members shall accept declarations and notices from car2go for authorized Drivers.</p>
          <p>7.3 Members may select a fee model from the options offered by car2go. Rentals shall always be settled according to the fee model selected for the settlement account that is responsible for fees incurred (regardless of who initiated the rental).</p>
        </blockquote>
        <p><b>Section 8 – Prices and Payments in Default</b></p>
        <blockquote>
          <p>8.1 Members must pay the prices set out in the Fee Schedule. The fee structures and prices have been set out in the Fee Schedule and include reference to community and venue taxes applicable in certain jurisdictions. Payment is due upon the Rental Period commencing.</p>
          <p>8.2 If the vehicle is not operational although a green status light indicated that it was &quot;available,&quot; the Member will not be charged for the rental fee.</p>
          <p>8.3Payments shall be made by credit card. The account from which the amount is to be collected shall have sufficient funds available to cover any charges. The Member is solely responsible for any associated credit card charges or fees.</p>
          <p>8.4If the Member defaults on any payments, car2go may charge reminder fees and default interest in accordance with the provisions of applicable law, which in any event shall not exceed 30% per annum.</p>
        </blockquote>
        <p><b>Section 9 – Obligations of Members and Drivers, Prohibitions</b></p>
        <blockquote>
          <p>9.1 Members and Drivers shall:</p>
          <ol type="a">
            <li>handle the vehicle gently and with care, and in particular observe the stipulations of the manufacturer's manual and the break-in rules, and adhere to applicable maximum and minimum speeds;</li>
            <li>immediately inform the Service Centre of any instances of major soiling, accident or damage to the vehicle resulting from force or accident, followed by reporting to police if required. (The Service Centre can be contacted free of charge by telephone using the telephone function installed in each vehicle.) At the request of car2go or its insurer, the Member must transmit to car2go a copy of any citation report or other documents related to the accident and in the Member or Driver's possession;</li>
            <li>secure the vehicle against theft (windows must be closed and central locking system enabled);</li>
            <li>ensure that the vehicle is used only in a state in which it is roadworthy and reliable;</li>
            <li>comply with all legal obligations in connection with the operation of the vehicle to the extent such obligations are not borne by car2go by reason of these Terms and Conditions;</li>
            <li>notify car2go of any tickets and/or warnings for traffic or driving offences received while operating a vehicle or during a Rental Period;</li>
            <li>keep any password and/or PIN numbers in strict confidence and not make passwords or PINs available to third parties, including noting passwords or PINs on or near the membership card. At no time should a password or PIN be recorded in written or electronic form. If a password or PIN is recorded in written or electronic form, the Member must pay for any associated costs and/or damages arising from unauthorized use by third parties. If a Member or Driver has reason to believe a third party may have obtained unauthorized knowledge of a password or PIN, the Member or Driver must change the compromised password or PIN; and</li>
            <li>permit car2go to record telephone conversations in connection with membership, in order to clear up legal issues and/or to improve the quality of its vehicles and services.</li>
          </ol>
          <p>9.2 The Driver shall not:</p>
          <ol type="a">
            <li>Using the car2go vehicle under the influence of alcohol, drugs or medication that could adversely impact his/her ability to drive; drinking alcohol and driving is strictly prohibited (for alcohol, a zero-tolerance policy is adopted and a limit of 0.0% applies);</li>
            <li>Allowing third-parties, other than the Member or Driver initiating the Rental Period, to drive a  vehicle (this also applies for persons who are themselves car2go Members or Drivers, but did not initiate the current rental);</li>
            <li>Using the fuel card in order to get gasoline for vehicles other than the car2go vehicle to which the fuel card is assigned;</li>
            <li>Using the car2go vehicle for cross-country drives, motor sports events, or races of any kind;</li>
            <li>Using the car2go vehicle for vehicle tests, driving classes, or in order to transport persons as part of a commercial operation;</li>
            <li>Using the car2go vehicle to transport flammable, poisonous, or otherwise dangerous goods for other than household use or in amounts greater than what is usually consumed in a household;</li>
            <li>Transporting objects with the  vehicle that could – due to their size, shape or weight – adversely impact the vehicle&rsquo;s handling safety or that could damage the interior of the  vehicle;</li>
            <li>Using the car2go vehicle in the commission of a crime;</li>
            <li>smoke or use tobacco products in the vehicle, or allow passengers to smoke or use tobacco products in the vehicle;</li>
            <li>with the exception of guide animals accompanying a Member, Driver or passenger with a disability (which guide animals must be stored in an appropriate animal carrier), take animals into the vehicle;</li>
            <li>excessively soil the vehicle or leave trash of any kind in the vehicle;</li>
            <li>remove objects that are attached to the vehicle or form part of the vehicle's equipment;</li>
            <li>Having more than two people sit in the  vehicle; and</li>
            <li>Personally performing or authorizing repairs or any conversions on or to the  vehicle.</li>
          </ol>
          <p>9.3 In the interests of all Members, Drivers, and the general public, the style of driving should be suited to improving fuel economy.</p>
        </blockquote>
        <p><b>Section 10 – Actions in the Event of Accidents, Damages, Defects, Repairs</b></p>
        <blockquote>
          <p>10.1 The Member is solely responsible for all damage to a vehicle (including major soiling) incurred during the Rental Period. If the Member or its authorized Driver is the last user of a vehicle before damage is reported, the Member shall be presumed to have caused and shall be responsible for the damage absent evidence to the contrary.</p>
          <p>10.2 The Driver must report accidents, defects or damage to the vehicle resulting from force or accident to the Service Centre, followed by reporting to police if required. (The Service Centre can be contacted free of charge by telephone using the telephone function installed in each vehicle.)</p>
          <p>10.3 If the police attend to the scene of an accident involving the vehicle, the Driver must remain at the accident site until the police have finished making a record of the accident and must take reasonable measures to conserve evidence and mitigate any damages. If the police do not attend at the accident site, the Driver must report the accident to the nearest collision reporting centre if required. In the event of an accident, the Driver may not acknowledge his or her culpability, may not accept or admit any liability, and may not make any similar declaration. The vehicle that has been in the accident may be parked only in an area that is sufficiently supervised and safe.</p>
          
         <p> 10.4	In all cases, car2go is entitled to any compensation of damages paid in connection with damages to a vehicle. Should such funds have been paid to the Member or to the Driver, the Member or Driver must transfer such funds to car2go.</p>
          
          <p>10.5 The Member and/or Driver are solely responsible for the consequences of traffic offences or criminal acts that are established to have been perpetrated in connection with use of the vehicle, and shall be responsible to car2go for any fees and costs car2go may incur in connection with same. In such events, car2go is under obligation to and may provide the name of the Driver to the appropriate authorities.</p>
          <p>10.6 Should car2go so demand, the Member and/or Driver must immediately provide information on the location of the vehicle and to enable inspection by car2go or other third parties designated by car2go.</p>
        </blockquote>
        <p><b>Section 11 – Termination, Ending of Rental Period</b></p>
        <blockquote>
          <p>11.1 car2go shall be entitled to terminate membership and/or driving privileges immediately if the Member and/or Driver:</p>
          <ol type="a">
            <li>Defaults on payments that are due;</li>
            <li>Ceases to make payments in general; </li>
            <li>Has made incorrect statements or has failed to disclose circumstances in the course of the rental relationship; </li>
            <li>fails to comply with any car2go rules or polices relating to operation of a vehicle;</li>
            <li>is convicted of a driving offence;</li>
            <li>no longer possess a valid driver's license; or</li>
            <li>does not cease and desist from material violations of this Agreement in spite of having been asked to do so in writing, or does not immediately remedy the consequences of such violations.</li>
          </ol>
          <p>11.2 If a membership is terminated in accordance with the above paragraph, car2go:</p>
          <ol type="a">
            <li>may have the vehicle returned immediately upon termination of the membership. If the Member fails to immediately return the vehicle, car2go may take possession of the vehicle at the Member's expense;</li>
            <li>is entitled to payment of any rental fees and/or associated costs until the vehicle is returned; and</li>
            <li>is entitled to compensation of damages. By way of compensating its damages, car2go will invoice the Member for specific damages resulting from the non-fulfillment of this Agreement.</li>
          </ol>
          <p>11.3 car2go or the Member may terminate this Agreement with or without cause at any time by providing written notice to the non-terminating party. Notwithstanding any termination, the Member and/or Driver will remain responsible for all charges, including account balances, registration fees and any damages and penalties incurred as of the date of termination or upon discovery by car2go.</p>
          <p>11.4<b> Cancellation Policy -</b> Members will only be able to obtain refunds for membership fees pursuant to their rights under applicable consumer protection legislation or at car2go&rsquo;s discretion.</p>
        </blockquote>
        <p><b>Section 12 – Insurance and Member/Driver Responsibility (including deductible)</b></p>
        <blockquote>
          <p>12.1 <b><u>MEMBER DEDUCTIBLE OBLIGATION</u></b>.  </a>car2go complies with applicable motor vehicle insurance laws.  If Member is in compliance with this Agreement and is age 19 years or older (or as otherwise agreed to by contract), at a minimum, car2go shall provide statutorily required coverage on car2go vehicles for claims and/or liabilities arising out of the use or operation of the vehicle by the Member and/or Driver up to and including an amount equal to the minimum levels required by law in the province in which the accident occurs. <b><u>Member and/or Driver will be responsible for any associated deductible as set out on the following car2go  website:</u></b> <b><a href="http://www.car2go.com/vancouver/en/rates/" target="_blank">http://www.car2go.com</a></b>, <b>as updated from time to time.</b> </p>
          <p>12.2 If at any time it is determined that losses will exceed the provincial minimum levels provided by car2go, the Member's and/or Driver's personal insurance policy will apply and the Member's or Driver's insurance carrier may be notified for assumption of all further liability claims.</p>
          <p>12.3 car2go is not responsible for any damage to, or loss or theft of, any personal property, whether the damage or theft occurs during or after termination of the Rental Period regardless of fault or negligence.</p>
        </blockquote>
        <p><b>Section 13 – Liability of the Member, Liquidated Damages, Exclusion </b></p>
        <blockquote>
          <p>13.1 The Member and/or Driver shall be responsible to car2go for theft, damages to, or the loss of, the vehicle, car2go's loss of vehicle use in cases of accidents, vehicles being impounded, and improper use resulting in a vehicle being out of service, and for damages suffered by a third party. The Member or Driver shall fully compensate car2go for such damages and losses, provided they were caused by the Member or Driver or by actions of a third party that are to be attributed to the Member. Furthermore, the Member shall be responsible to car2go for the full amount of damages if the vehicle has been damaged or lost or if a third party has suffered damages by the fact that the Member or third parties for whom the Member is responsible have culpably violated the Agreement or the stipulations of applicable law and have thus adversely impacted insurance coverage. If an uninsured Member and/or Driver is liable, the Member and/or Driver shall indemnify and hold harmless car2go against any claims by third parties.</p>
          <p>13.2 The Member and/or Driver must pay liquidated damages if he/she has permitted an unauthorized person to drive the vehicle. car2go reserves the right to claim further damages. In such event, the liquidated damages will be offset.</p>
          <p>13.3 In the event of material violations of this Agreement, including any default on payments, car2go may exclude, with immediate effect, the Member or, as applicable, the Driver from the use of the vehicle and may deactivate the access means (RFID chip); this exclusion may be temporary or permanent. The Member or, as the case may be, the Driver, will be informed of his/her exclusion by e-mail.</p>
          <p>13.4 <b>Indemnification</b> <b>by</b> <b>Member -</b> The Member and Driver shall indemnify and hold car2go, its parent and affiliates and their respective directors, officers, employees, shareholders, agents, lawyers, assigns and successors-in-interest, harmless for all losses, liabilities, damages, injuries, claims, demands, costs, legal costs, and other expenses incurred by car2go in any manner from the car2go car-sharing program and based upon the Member's non-compliance with this Agreement, or from the use of the vehicle by the Member or any other person, including claims of, or liabilities to, third parties. The Member or Driver may present a claim to the Member's or Driver's insurance carrier for such events or losses; but in any event, the Member and/or Driver shall have final responsibility to car2go for all such losses. This obligation may be limited to the extent car2go's minimum financial responsibility coverage applies.</p>
        </blockquote>
        <p><b>Section 14 – Data Protection </b></p>
        <blockquote>
          <p>14.1 car2go collects, stores, and uses the Member's and Driver's personal information as that term is defined in the Privacy Policy, including the usage and vehicle data as they relate to the Member and Driver, to the extent this is necessary for the purpose of implementing the terms and conditions of this Agreement and the Member or Driver relationship and in accordance with the Privacy Policy.</p>
          <p>14.2 Should, in the context of the Member, Driver, rental, or registration relationship, third-party services be used, car2go is entitled to forward to the third-party service provider the Member's and/or Driver's personal information, to the extent this is required in order to fulfill membership or use requirements, in accordance with the Privacy Policy.</p>
          <p>14.3 Rental Periods shall be itemized in a statement and made available to Members via private web portal access at www.car2go.com. The statement will include the rental commencement time, point of departure and termination, the duration of the rental, distance driven and associated costs. Should a Member or Driver have his/her rentals charged to the settlement account of a different Member, this information shall be itemized on the statement with reference to the applicable Driver.</p>
          <p>14.4 car2go shall implement and maintain reasonable procedures for protecting personal information in compliance with applicable law and the Privacy Policy.</p>
        </blockquote>
        <p><b>Section 15 – General </b></p>
        <blockquote>
          <p>15.1 <b>Refund Policy - </b>Fees for use of vehicles are incurred as provided in this Agreement, the Rental Process terms and the Fee Schedule. Daily rental history and fees assessed by car2go to a Member's account can be viewed by accessing Member account information at www.car2go.com. car2go will also generate usage statements summarizing fees assessed to a Member. The usage statements can also be viewed and downloaded by accessing Member account information at www.car2go.com.<br>
            Members are on notice of all fees assessed at the time the usage statement is made available at www.car2go.com. Any request for refund of fees assessed to a Member's account must be received within 14 days of the date the usage statement is made available at www.car2go.com. Refunds are subject to review and approval on a case-by-case basis at car2go's sole discretion. No portion of the registration fee shall be refunded.</p>
          <p>15.2 <b>Choice of Law -</b> All items and conditions of these Terms and Conditions shall be interpreted, construed and enforced pursuant to the laws of the province where the vehicle is located at commencement of the Rental Period and the laws of Canada applicable in the province.</p>
          <p>15.3 <b>Conflicts - </b>In the event of any conflict between these Terms and Conditions and any other document with respect to the car2go car-sharing program, these Terms and Conditions shall govern except as otherwise set forth by car2go in writing.</p>
          <p>15.4 <b>User Guide, Parental Consent Forms, Application -</b>these Terms and Conditions, together with the User Guide, Rental Process terms, member application and any applicable consent forms constitute the entire agreement between the Member and car2go and will become a binding agreement between the Member and any authorized Drivers upon acceptance by the Member. Should any individual terms or provisions in these Terms and Conditions become ineffective, this shall not impact the effectiveness of the remaining terms of these Terms and Conditions.</p>
          <p>15.5 <b>Modifications - </b>Any modifications to these Terms and Conditions are available at www.car2go.com and are deemed approved by the Member unless written notice of objection is received in writing by car2go. Any notice of objection is deemed effective upon written confirmation of receipt from car2go to the Member.</p>
          <p>15.6 <b>Questions - </b>Comments or questions regarding these Terms and Conditions should be directed to car2go. Visit www.car2go.com for available methods of contact.</p>
          </p>
          
          
        </blockquote>
        <p>These Terms and Conditions are effective April 26, 2012.</p>
        
        </td>
      <td></td>
    </tr>
    <tr valign="top">
      <td width="21" height="21"><img src="imgs/corner_left_bottom.png" width="21" height="21" alt="Toronto Car Sharing"></td>
      <td bgcolor="#E3F5FC"></td>
      <td width="21" height="21"><img src="imgs/corner_right_bottom.png" width="21" height="21" alt="Toronto Car Sharing"></td>
    </tr>
  </table>
</div>
<?php require_once('footer.php'); ?>