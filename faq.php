<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>car2go Toronto | Toronto Car Sharing | Rideshare In Toronto</title>
<link rel="Stylesheet" type="text/css" href="style.css" title="Menu Styles">
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<meta http-equiv="PRAGMA" content="NO-CACHE">
<meta http-equiv="Expires" content="0">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="imagetoolbar" content="no">
<meta name="description" content="car2go">
<meta name="keywords" content="car2go, smart, Toronto">
<link rel="shortcut icon" href="http://www.car2go.com/favicon.ico" type="image/x-icon">
<link rel="icon" href="http://www.car2go.com/favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="shadowbox.css">

<script type="text/javascript" src="shadowbox.js"></script>
<script type="text/javascript">
Shadowbox.init({
    modal: true
});
</script>
<script src="SpryAssets/SpryEffects.js" type="text/javascript"></script>
<script type="text/javascript">
<!--
function MM_validateForm() { //v4.0
  if (document.getElementById){
    var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
    for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=document.getElementById(args[i]);
      if (val) { nm=val.name; if ((val=val.value)!="") {
        if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
          if (p<1 || p==(val.length-1)) errors+='- '+nm+' must contain an e-mail address.\n';
        } else if (test!='R') { num = parseFloat(val);
          if (isNaN(val)) errors+='- '+nm+' must contain a number.\n';
          if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
            min=test.substring(8,p); max=test.substring(p+1);
            if (num<min || max<num) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
      } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' is required.\n'; }
    } if (errors) alert('The following error(s) occurred:\n'+errors);
    document.MM_returnValue = (errors == '');
} }
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
function MM_effectAppearFade(targetElement, duration, from, to, toggle)
{
	Spry.Effect.DoFade(targetElement, {duration: duration, from: from, to: to, toggle: toggle});
}

function showonlyone(thechosenone) {
      var answer = document.getElementsByTagName("div");
            for(var x=0; x<answer.length; x++) {
                  name = answer[x].getAttribute("name");
                  if (name == 'answer') {
                        if (answer[x].id == thechosenone) {
                        answer[x].style.display = 'block';
                  }
                  else {
                        answer[x].style.display = 'none';
                  }
            }
      }
}
<!--
var swf ='<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0" width="321" height="275"><param name="movie" value="car2go-HowItWorks2.swf"><param name="quality" value="high"><embed src="car2go-HowItWorks2.swf" width="321" height="275" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash"></embed></object>';

function flashSwap() 
{
	document.getElementById("movie").innerHTML = swf;
}
//-->
</script>
<style type="text/css" media="screen">
#flashContent {
	width:100%;
	height:100%;
}
</style>

</head>
<body id="index">
<div align="center">
  <table border="0" cellpadding="5" cellspacing="0" width="950">
    <tr>
      <td width="675">&nbsp;</td>
      <td align="right" valign="bottom"><iframe src="http://www.facebook.com/plugins/like.php?app_id=191729397539808&amp;href=http%3A%2F%2Fwww.facebook.com%2Fcar2go.austin&amp;send=false&amp;layout=button_count&amp;width=100&amp;show_faces=true&amp;action=like&amp;colorscheme=light&amp;font=arial&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:100px; height:21px;" allowTransparency="true"></iframe>
        
        <!-- AddThis Button BEGIN -->
        
    <?php require_once('add_this.php'); ?>
        <!-- AddThis Button END --></td>
    </tr>
  </table>
  <table border="0" cellpadding="0" cellspacing="0" width="950">
    <tr>
      <td width="21" height="21"><img src="imgs/corner_left_top.png" width="21" height="21" alt="Toronto Car Sharing"></td>
      <td bgcolor="#ffffff" width="908"></td>
      <td width="21" height="21"><img src="imgs/corner_right_top.png" width="21" height="21" alt="Toronto Car Sharing"></td>
    </tr>
    
    <tr>
      <td width="21" bgcolor="#FFFFFF"></td>
      <td bgcolor="#ffffff" width="908" style="padding-bottom:0px;"><table style="display: inline-table;" bgcolor="#009bda" border="0" cellpadding="0" cellspacing="0" width="908">
          <tr bgcolor="#FFFFFF">
            <td valign="top"><a href="index.php"><img name="car2gologo_s1" src="imgs/car2go-logo_s1.jpg" width="136" height="95" border="0" id="car2gologo_s1" alt="" /></a><br /></td>
            <td valign="top"><a href="simple.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('simplenav_s1','','imgs/simple-nav_s2.jpg',1);"><img name="simplenav_s1" src="imgs/simple-nav_s1.jpg" width="142" height="95" border="0" id="simplenav_s1" alt="" /></a></td>
            <td valign="top"><a href="affordable.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('affordablenav_s1','','imgs/affordable-nav_s2.jpg',1);"><img name="affordablenav_s1" src="imgs/affordable-nav_s1.jpg" width="145" height="95" border="0" id="affordablenav_s1" alt="" /></a></td>
            <td valign="top"><a href="sustainable.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('sustainablenav_s1','','imgs/sustainable-nav_s2.jpg',1);"><img name="sustainablenav_s1" src="imgs/sustainable-nav_s1.jpg" width="145" height="95" border="0" id="sustainablenav_s1" alt="" /></a></td>
            <td valign="top"><a href="howto.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('howtonav_s1','','imgs/howto-nav_s2.jpg',1);"><img name="howtonav_s1" src="imgs/howto-nav_s1.jpg" width="145" height="95" border="0" id="howtonav_s1" alt="" /></a></td>
            <td valign="top" align="right" bgcolor="#FFFFFF">&nbsp;</td>
          </tr>
        </table>
      
      </td>
      <td width="21" bgcolor="#FFFFFF"></td>
    </tr>
    
    <tr valign="top">
      <td width="950" colspan=3 valign="top" bgcolor="#ffffff"><div class="padtop padbot">
          <table cellpadding="0" cellspacing="0" border="0" width="950">
            <tr>
              <td width="21" bgcolor="#FFFFFF"></td>
              <td valign="top" style="padding-right:25px;"><a href="member.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('signuplink_s1','','imgs/signup-link_s2.jpg',1);"><img name="signuplink_s1" id="signuplink_s1" src="imgs/signup-link_s1.jpg" width="440" height="60" alt="Sign Up Now" border="0"></a></td>
              <td valign="top"><a href="drive.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('memberinfolink_s1','','imgs/memberinfo-link_s2.jpg',1);"><img name="memberinfolink_s1" id="memberinfolink_s1" src="imgs/memberinfo-link_s1.jpg" width="440" height="60" alt="Want to Learn More?" border="0"></a></td>
              <td width="21" bgcolor="#FFFFFF"></td>
            </tr>
          </table>
      </div></td>
    </tr>
    <tr bgcolor="#E3F5FC">
      <td></td>
      <td class="padtop"><h3>Toronto car2go FAQs </h3>
        <p>Our aim is to offer the highest level of service both on and off the street. To better serve you, some answers to common questions are listed below. If you need further information contact our service center at 1.877.488.4224 or email us at <a href="mailto:Toronto@car2go.com">Toronto@car2go.com</a>.</p>
        <table cellpadding="10" cellspacing="0" border="0" width="908">
          <tr>  
            <td width="440" valign="top" class="questions"><a href="#answer1" onClick="showonlyone('answer1')">1. What is car2go?</a><br>
              <a href="#answer2" onClick="showonlyone('answer2')">2. How do I use a car2go?</a><br>
             <a href="#answer3" onClick="showonlyone('answer3')">3. What kind of vehicles does car2go use?</a><br>
             <a href="#answer4" onClick="showonlyone('answer4')">4. What features are available in a car2go?</a><br>
             
             
              <a href="#answer5" onClick="showonlyone('answer5')">5. Who can register for car2go?</a><br>
              <a href="#answer6" onClick="showonlyone('answer6')">6. How can I register?</a><br>
              <a href="#answer7" onClick="showonlyone('answer7')">7. Do I need to reserve a car2go?</a><br>
              <a href="#answer8" onClick="showonlyone('answer8')">8. Can my pet ride with me?</a><br>
              <a href="#answer9" onClick="showonlyone('answer9')">9. Can I smoke in the car? </a><br>
              
              
              <a href="#answer10" onClick="showonlyone('answer10')">10. Can my children ride in a car2go?</a><br>
              </td>
            <td width="20"></td>
            <td width="440" valign="top" class="questions">
            <a href="#answer11" onClick="showonlyone('answer11')">11. What is the insurance coverage? </a><br>
            <a href="#answer12" onClick="showonlyone('answer12')">12. What do I do if I need roadside assistance or get in an accident?</a><br>
              <a href="#answer13" onClick="showonlyone('answer13')">13. Can I go outside of the Home Area with the car? </a><br>
              <a href="#answer14" onClick="showonlyone('answer14')">14. How can I tell if a car2go is available?</a><br>
              
              
              <a href="#answer15" onClick="showonlyone('answer15')">15. How do I unlock the car2go?</a><br>
              <a href="#answer16" onClick="showonlyone('answer16')">16. How do I activate the car2go and where is the key?</a><br>
              <a href="#answer17" onClick="showonlyone('answer17')">17. Where do I park the car2go?</a><br>
              <a href="#answer18" onClick="showonlyone('answer18')">18. What do I do when I am finished using car2go?</a><br>
              <a href="#answer19" onClick="showonlyone('answer19')">19. How do I pay?</a><br></td>
          </tr>
        </table>
        <div class="qaholder">
        <a name="answer1"></a><div name="answer" id="answer1" class="faq"><b>1. What Is car2go? </b><br>
    car2go is a groundbreaking mobility program redefining transportation in rapidly growing urban areas. Launched by Daimler in the southern German city of Ulm during March 2009 and in Austin, Texas, in November 2009, it provides a fleet of free-floating, low-emission, self-service smart fortwo cars distributed all over the city. car2go vehicles can be accessed "on-demand" or reserved for up to 24 hours in advance. For "on-demand" access, members simply swipe their membership card against the vehicle's windshield. Unlike traditional carsharing programs, car2go allows its members to use the vehicle for as long as they like, without committing to a specific return time or location. They can finish the rental in any authorized parking space within the car2go business area. Attractive "by-the-minute" rates include costs for fuel, insurance, parking, and maintenance.
 
        </div>
        <a name="answer2"></a><div name="answer" id="answer2" class="faq"><b>2. How do I use a car2go? </b><br>
       You can keep the vehicle as long as you want - you can use it for short trips in the city, drive to the countryside, or go to nearby towns. Of course, you can make intermediate stops whenever you want and leave the car2go – for example, to go shopping. In this case, use the remote access on the key in order to lock and unlock the vehicle. The system therefore registers that you are making an intermediate stop, and will block the vehicle in your absence so you can continue using it when you return. This will be indicated by a red blinking signal on the reader.

        </div>
        <a name="answer3"></a><div name="answer" id="answer3" class="faq"><b>3. What kind of vehicles does car2go use? </b><br>
        car2go uses the fuel-efficient, low-emission smart fortwo vehicle. All smart fortwos come packed with standard safety features that are more likely to be found in luxury vehicles. These include the protective tridion safety cell developed especially for the smart fortwo to help ensure crash compatibility with larger passenger cars, electronic stability program, anti-lock braking system, and four airbags, to name a few.</div>
        
        <a name="answer4"></a><div name="answer" id="answer4" class="faq"><b>4. What features are available in a car2go? </b><br>
          Insurance, charging, roadside assistance, free parking in designated areas, GPS navigation, maintenance, and 24/7 customer support are all included in the standard rental price. Inside the car, the onboard touchscreen computer makes operating the vehicle even easier. PIN entry, evaluation, call center team contact, the radio, and navigation system can be intuitively controlled using the touchscreen. The PIN you set during registration needs to be typed in before you can drive the car; this ensures that should your membership card be misplaced, no one can drive your car2go but you. The customer support function enables customers to have immediate live in-car support should they need it.

        </div>
        <a name="answer5"></a><div name="answer" id="answer5" class="faq"><b>5. Who can register for car2go? </b><br>
       Anyone! There are just a few requirements members need to meet to be covered under our insurance. A member needs three or more years of driving experience; so members cannot have a learner's permit or be a "new" driver. Also, a member must have a driver's abstract in good standing with no alcohol or drug prohibitions. Offences under the Motor Vehicle Code will be examined--the severity of which can negate membership.
         
          </div>
        <a name="answer6"></a><div name="answer" id="answer6" class="faq"><b>6. How can I register? </b><br>
    Getting into your first car2go is simple. Just go to the registration form on any page and fill out your personal and billing information on our secure web page.  A one-time payment of $35 for your membership will be processed only if you are accepted into the program.  After registration, your welcome kit and membership card will arrive within five days. The kit will include some quick tips on how to get started with car2go.  </div>
      
        <a name="answer7"></a><div name="answer" id="answer7" class="faq"><b>7. Do I need to reserve a car2go? </b><br>
       Not at all! Our free-floating fleet is positioned around the city for any spur of the moment decision to get in and drive. Should you want to reserve a car, you can find one on our iPhone application, log into My car2go on the car2go.com website, contact the car2go customer call center up to 24 hours in advance. Normally, within the urban area of Toronto, the nearest car2go is never too far away. Only during periods of extremely high use could the distance sometimes be somewhat greater. Should you leave the car very messy, a cleaning charge may apply if cleaning above the normal scope of duties is required. Lastly, you never have to reserve or return a car2go; you can use it for as long as you like and leave it anywhere you like provided you are legally parked in an authorized spot within the Home Area.


        </div>
        
        
        <a name="answer8"></a><div name="answer" id="answer8" class="faq"><b>8. Can my pet ride with me? </b><br>
        Sadly no. Your beloved pets cannot join you on your trip. This is for your safety as well as your pets. </div>
        
        <a name="answer9"></a><div name="answer" id="answer9" class="faq"><b>9. Can I smoke in the car? </b><br>
          No. As a shared vehicle, car2go cannot facilitate smokers. Better to have that last puff before you get in.</div>
          
          
          
        
        <a name="answer10"></a><div name="answer" id="answer10" class="faq"><b>10.  Can my children ride in a car2go? </b><br>
          The smart fortwo features highly efficient restraint systems that meet Mercedes-Benz's passive safety standards. Infant or children restraint systems (booster, infant or child seat) can be affixed properly to the passenger seat by means of the safety belt. A ratchet mechanism locates the tightly pulled safety belt and holds the infant/child restraint system in position.  An electronically controlled system will detect the weight of the occupant on the passenger seat or the installation of a child restraint system. The Electronic Control Unit (ECU) determines the appropriate mode of the dual stage full size front airbag deployment. For very young passengers for whom an airbag deployment could mean a serious risk, the front and side airbags will be deactivated. A control lamp in the center of the roof panel will show this clearly. Always check the control lamp to make sure the airbags are deactivated for very young passengers. It should be glowing and say "passenger side airbag off."</div>
          
          
          
            
        <a name="answer11"></a><div name="answer" id="answer11" class="faq"><b>11. What is the insurance coverage? </b><br>
        Base membership coverage includes the following limits: Liability of $100,000 Bodily Injury per person / $300,000 Bodily Injury per accident, and $50,000 Physical Damage per accident. Coverage for the car2go vehicle is provided with a deductible of $1,000 per occurrence.  </div>
          
          <a name="answer12"></a><div name="answer" id="answer12" class="faq"><b>12. What do I do if I need roadside assistance or get in an accident? </b><br>
         Call the customer service center at 855.454.1002 or press SOS on the touchscreen to report the incident. A representative will then immediately help you. An Accident Instructional Kit is located in the glove compartment with further directions.</div>
        
        
            <a name="answer13"></a><div name="answer" id="answer13" class="faq"><b>13. Can I go outside of the Home Area with the car? </b><br>
         YYes! An alert will sound to inform you of when you are leaving the Home Area. Once outside of the Home Area, you will not be able to end your rental. This means that you will be billed normally for the time you are outside of the Home Area until you return the vehicle to the area. For a map of the Home Area, <a href="TorontoHomeArea.pdf" target="_blank">click here</a>. </div>
         
         
         <a name="answer14"></a><div name="answer" id="answer14" class="faq"><b>14. How can I tell if a car2go is available? </b><br>
         Each car2go has a special card reader installed on its windshield. A light signal on the device displays whether the vehicle is currently available or reserved. Green is the color that identifies the car2go is available for use.</div>
         
         <a name="answer15"></a><div name="answer" id="answer15" class="faq"><b>15. How do I unlock the car2go? </b><br>
         When you register with car2go, you will be sent the car2go member card. In order to unlock the vehicle, simply hold up your member card close to the reader on the windshield. The reader will quickly indicate the doors are unlocked so that you can get in and go.</div>
         
         <a name="answer16"></a><div name="answer" id="answer16" class="faq"><b>16. How do I activate the car2go and where is the key? </b><br>
         After you have entered the car2go, you will need to enter your personal code (PIN) into the touch screen, and also assess the cleanliness of the vehicle and confirm that it is not damaged. Once you complete those questions, you will be prompted to accept car2go's terms and conditions and access the ignition key located to the right of the touch screen. The ignition is where the park brake and gear shift is located.</div>
         
         
         
        <a name="answer17"></a><div name="answer" id="answer17" class="faq"><b>17. Where do I park the car2go?</b><br>
       For a full list of parking rules, <a href="imgs/toronto_how2_pg2.jpg" rel="shadowbox">click here</a>. 

</div>


 <a name="answer18"></a><div name="answer" id="answer18" class="faq"><b>18. What do I do when I am finished using car2go? </b><br>
        When you are finished with your car2go, you must park in an authorized location or in a designated car2go parking space. Once parked, select "end rental" on the touch screen. Then, place the ignition key back in the touch screen. Once everything has been positioned, an integrated sensor will make sure that the rental process can be ended. You may exit the vehicle and lock it by holding the car2go membership card close to the reader on the windshield. The reader will indicate that the rental is completed and will automatically lock the doors. Check to ensure the doors have locked. If the doors do not lock, please contact the car2go team.
</div>
        
        
      
        <a name="answer19"></a><div name="answer" id="answer19" class="faq"><b>19. How do I pay?<br>
          </b>Payments for your rental will be applied to the credit or debit card you used in registration. All of your trips for the last month will be logged with attendant invoices; these are accessible in MY CAR2GO when logging in to the website.</div>
        <br>
        </div>
        </td>
      <td></td>
    </tr>
    <tr valign="top">
      <td width="21" height="21"><img src="imgs/corner_left_bottom.png" width="21" height="21" alt="Toronto Car Sharing"></td>
      <td bgcolor="#E3F5FC"></td>
      <td width="21" height="21"><img src="imgs/corner_right_bottom.png" width="21" height="21" alt="Toronto Car Sharing"></td>
    </tr>
  </table>
</div>
<?php require_once('footer.php'); ?>