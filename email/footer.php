<div class="footer">
  <ul>
    <li><a href="../faq.php">FAQ</a></li>
    <li><a href="../contact.php">Contact</a></li>
    <li><a href="../privacy.php">Privacy Policy</a></li>
    <li><a href="http://car2go.com" target="_blank">car2go.com</a></li>
  </ul>
</div>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-29817064-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</body>
</html>