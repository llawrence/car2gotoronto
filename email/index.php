<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>car2go Portland | Portland Car Sharing | Rideshare In Portland</title>
<meta name="description" content="Car sharing has come to town; Portland car2go means car rental by the minute for modern urban mobility.">
<meta id="MetaKeywords" name="KEYWORDS" content=" car2go Portland, Portland car2go, car to go, car rental, Portland car rental" />
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<meta http-equiv="PRAGMA" content="NO-CACHE">
<meta http-equiv="Expires" content="0">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="imagetoolbar" content="no">
<link rel="shortcut icon" href="http://www.car2go.com/favicon.ico" type="image/x-icon">
<link rel="Stylesheet" type="text/css" href="../style.css" title="Menu Styles">
<link rel="icon" href="http://www.car2go.com/favicon.ico" type="image/x-icon">

<script src="../SpryAssets/SpryEffects.js" type="text/javascript"></script>
<script src="../Scripts/swfobject_modified.js" type="text/javascript"></script>
<script type="text/javascript">

<!--
function MM_validateForm() { //v4.0
  if (document.getElementById){
    var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
    for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=document.getElementById(args[i]);
      if (val) { nm=val.name; if ((val=val.value)!="") {
        if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
          if (p<1 || p==(val.length-1)) errors+='- '+nm+' must contain an e-mail address.\n';
        } else if (test!='R') { num = parseFloat(val);
          if (isNaN(val)) errors+='- '+nm+' must contain a number.\n';
          if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
            min=test.substring(8,p); max=test.substring(p+1);
            if (num<min || max<num) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
      } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' is required.\n'; }
    } if (errors) alert('The following error(s) occurred:\n'+errors);
    document.MM_returnValue = (errors == '');
} }
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
function MM_effectAppearFade(targetElement, duration, from, to, toggle)
{
	Spry.Effect.DoFade(targetElement, {duration: duration, from: from, to: to, toggle: toggle});
}


<!--
var swf ='<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0" width="321" height="275"><param name="movie" value="../car2go-HowItWorks2.swf"><param name="quality" value="high"><embed src="../car2go-HowItWorks2.swf" width="321" height="275" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash"></embed></object>';

function flashSwap() 
{
	document.getElementById("movie").innerHTML = swf;
}
//-->
</script>
<style type="text/css" media="screen">
#flashContent {
	width:100%;
	height:100%;
}
</style>

<script src="http://www.apple.com/library/quicktime/scripts/ac_quicktime.js" language="JavaScript" type="text/javascript"></script>
<script src="http://www.apple.com/library/quicktime/scripts/qtp_library.js" language="JavaScript" type="text/javascript"></script>
<link href="http://www.apple.com/library/quicktime/stylesheets/qtp_library.css" rel="StyleSheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../shadowbox.css">

<script type="text/javascript" src="../shadowbox.js"></script>
<script type="text/javascript">
Shadowbox.init({
    modal: true
});
</script>

</head>

<body id="index" onload="MM_preloadImages('../imgs/simple-nav_s2.jpg','../imgs/convenient-nav_s2.jpg','../imgs/affordable-nav_s2.jpg','../imgs/sustainable-nav_s2.jpg','../imgs/member-link_s2.jpg','../imgs/drive-nav_s2.jpg')">
<div align="center"><img src="https://secure.fastclick.net/w/tre?ad_id=24878;evt=17776;cat1=22350;cat2=22351;rand=[CACHEBUSTER]" width="1" height="1" border="0">
  <table border="0" cellpadding="5" cellspacing="0" width="950">
    <tr>
      <td width="675">&nbsp;</td>
      <td align="right" valign="bottom"><iframe src="http://www.facebook.com/plugins/like.php?app_id=191729397539808&amp;href=http%3A%2F%2Fwww.facebook.com%2Fcar2go.austin&amp;send=false&amp;layout=button_count&amp;width=100&amp;show_faces=true&amp;action=like&amp;colorscheme=light&amp;font=arial&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:100px; height:21px;" allowTransparency="true"></iframe>
        
        <!-- AddThis Button BEGIN -->
        <?php require_once('../add_this.php'); ?>
        <!-- AddThis Button END --></td>
        
    </tr>
  </table>
  
  <table border="0" cellpadding="0" cellspacing="0" width="950">
    <tr>
      <td width="21" height="21"><img src="../imgs/corner_left_top.png" width="21" height="21" alt="Portland Car Sharing"></td>
      <td bgcolor="#ffffff" width="908"></td>
      <td width="21" height="21"><img src="../imgs/corner_right_top.png" width="21" height="21" alt="Portland Car Sharing"></td>
    </tr>
    <tr>
      <td width="21" bgcolor="#FFFFFF"></td>
      <td bgcolor="#ffffff" width="908"><table style="display: inline-table;" bgcolor="#009bda" border="0" cellpadding="0" cellspacing="0" width="908">
          <tr bgcolor="#FFFFFF">
            <td valign="top"><a href="../index.php"><img name="car2gologo_s1" src="../imgs/car2go-logo_s1.jpg" width="136" height="95" border="0" id="car2gologo_s1" alt="" /></a><br /></td>
            <td valign="top"><a href="../simple.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('simplenav_s1','','../imgs/simple-nav_s2.jpg',1);"><img name="simplenav_s1" src="../imgs/simple-nav_s1.jpg" width="142" height="95" border="0" id="simplenav_s1" alt="" /></a></td>
            <td valign="top"><a href="../convenient.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('convenientnav_s1','','../imgs/convenient-nav_s2.jpg',1);"><img name="convenientnav_s1" src="../imgs/convenient-nav_s1.jpg" width="145" height="95" border="0" id="convenientnav_s1" alt="" /></a></td>
            <td valign="top"><a href="../affordable.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('affordablenav_s1','','../imgs/affordable-nav_s2.jpg',1);"><img name="affordablenav_s1" src="../imgs/affordable-nav_s1.jpg" width="145" height="95" border="0" id="affordablenav_s1" alt="" /></a></td>
            <td valign="top"><a href="../sustainable.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('sustainablenav_s1','','../imgs/sustainable-nav_s2.jpg',1);"><img name="sustainablenav_s1" src="../imgs/sustainable-nav_s1.jpg" width="145" height="95" border="0" id="sustainablenav_s1" alt="" /></a></td>
            <td valign="top" align="right" bgcolor="#FFFFFF">&nbsp;</td>
          </tr>
        </table>
      
      </td>
      <td width="21" bgcolor="#FFFFFF"></td>
    </tr>
    
    <tr valign="top">
      <td width="950" colspan=3 valign="top" bgcolor="#ffffff"><div id="flashContent">
          <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="950" height="265" id="vancouver-homecar" align="top">
            <param name="movie" value="../pdx-homecar.swf" />
            <param name="quality" value="high" />
            <param name="bgcolor" value="#ffffff" />
            <param name="play" value="true" />
            <param name="loop" value="true" />
            <param name="wmode" value="transparent" />
            <param name="scale" value="showall" />
            <param name="menu" value="true" /> 
            <param name="devicefont" value="false" />
            <param name="salign" value="" />
            <param name="allowScriptAccess" value="sameDomain" />
            <!--[if !IE]>-->
            <object type="application/x-shockwave-flash" data="../pdx-homecar.swf" width="950" height="265">
              <param name="movie" value="../pdx-homecar.swf" />
              <param name="quality" value="high" />
              <param name="bgcolor" value="#ffffff" />
              <param name="play" value="true" />
              <param name="loop" value="true" />
              <param name="wmode" value="transparent" />
              <param name="scale" value="showall" />
              <param name="menu" value="true" />
              <param name="devicefont" value="false" />
              <param name="salign" value="" />
              <param name="allowScriptAccess" value="sameDomain" />
              <!--<![endif]--> 
              <img src="../imgs/static-background.png" width="950" height="265" alt="car2go"> 
              <!--[if !IE]>-->
            </object>
            <!--<![endif]-->
          </object>
        </div></td>
    </tr>
    
 <tr valign="top">
      <td width="950" colspan=3 valign="top" bgcolor="#ffffff">
      <div class="padtop padbot">
          
          <table cellpadding="0" cellspacing="0" border="0" width="950">
            <tr>
              <td width="21" bgcolor="#FFFFFF"></td>
              <td valign="top"><a href="../member.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('memberlink_s1','','../imgs/member-link_s2.jpg',1);"><img name="memberlink_s1" src="../imgs/member-link_s1.jpg" width="453" height="73" border="0" id="memberlink_s1" alt="" /></a></td>
              <td valign="top"><a href="../drive.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('drivenav_s1','','../imgs/drive-nav_s2.jpg',1);"><img name="drivenav_s1" src="../imgs/drive-nav_s1.jpg" width="455" height="73" border="0" id="drivenav_s1" alt="" /></a></td>
              <td width="21" bgcolor="#FFFFFF"></td>
            </tr>
          </table>
          </div><!-- end padding -->
        </td>
    </tr>
    
    <tr valign="middle" bgcolor="#E3F5FC" border="0">
      
      <td colspan="3" align="center">
      <table border="0" cellpadding="0" cellspacing="0" width="900" align="center">
      
       <tr>
      	<td valign="top" align="center" class="padtop" width="440" border="0" style="margin-right:20px;">
          
        <div class="topinset"> 
           <div class="bottominset" style="text-align:center; height: 421px;">  
           
			<?php require_once('ad.php'); ?>
			
        	</div><!-- end div bottominset -->  
        </div><!-- end div topinset -->
         
       
        </td>
        
          
        
      <td class="padtop" align="center" width="440" valign="top">
      
      <table width="440" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td valign="top" width="440">
            	<div class="topinset">
                
				<?php require_once('order.php'); ?>
                
                </div><!-- end topinset -->
                </td>
          </tr>
        </table>
        
        </td><!-- ends td that holds 2 tables -->
         
        	</tr>
          
            </table>
        </td><!-- end td colspan 3 that holds top 2 boxes -->
    </tr>
    
    <tr valign="top" bgcolor="#E3F5FC">
     <td></td>
      <td class="hometext"><p><b>Welcome to car2go Portland</b> <br>We are thrilled to become part of such a diverse cultural and business hub. We&rsquo;re looking forward to becoming an integral part of Portland&rsquo;s green mobility strategy and helping you meet your mobility needs in a more affordable and sustainable manner.       
        <p><b>What is car2go?</b> <br>
          Unlike traditional carsharing, car2go is not station-based. A network of 200 car2gos is located throughout the downtown Portland area and can be accessed on demand. </p>
        <p>When you use a car2go, you&rsquo;re free to drive the vehicle for as long as you like, without committing to a specific return time. And you can finish the rental in any available public parking space within the <a href="../PortlandHomeArea.pdf" target="_blank">Home Area</a>. For more parking information, <a href="../imgs/how2_pg2.jpg" rel="shadowbox">click here</a>.</p>    
        <p>Charges are based only on the minutes you use and include insurance, maintenance and parking.</p>
      	<p>Want to learn more about car2go?&nbsp;<a href="http://www.car2go.com/portland/en/concept/idea/">Click here</a>.</p></td>
      <td></td>
    </tr>
    <tr valign="top" bgcolor="#E3F5FC">
      <td></td>
      <td align="center" class="padbot">
      	<table width="908" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td valign="top" width="440" align="center">
            	<table width="440" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff" >
          			<tr>
            			<td valign="top" width="440" align="center">
                
                <div class="topinset">
                	<div class="bottominset" style="text-align:center;">
               			
                     <a href="../imgs/how2_pg1.jpg" rel="shadowbox[how2]"><img src="../imgs/how2_tout.jpg" alt="" width="390" height="390"></a>
                     <a href="../imgs/how2_pg2.jpg" rel="shadowbox[how2]"><img src="../imgs/how2_pg2.jpg" alt="" width="400" height="400" class="how2_gal"></a>
                     <a href="../imgs/how2_pg3.jpg" rel="shadowbox[how2]"><img src="../imgs/how2_pg3.jpg" alt="" width="400" height="400" class="how2_gal"></a>

                     
                     </div>
                  <div style="clear:both;font-size:0px;line-height:0px;"><!--spacing div --></div></div></td>
                </tr>
                
            </table>
            <td width="20"></td>
            <td valign="top">
            	<div class="topinset">
                	<div class="bottominset"> 
                  <!-- video here -->
                  <table border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#ffffff">  
				  <tr>
					<td valign="top" style="height: 390px">   
					
			  		  <iframe src="//www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2Fcar2go.Portland&amp;width=400&amp;colorscheme=light&amp;show_faces=true&amp;border_color=none&amp;stream=false&amp;header=false&amp;height=360" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:400px; height:360px;" allowTransparency="true"></iframe>

		 
				   </td>
				   </tr>
				   </table><!-- close concept works table -->
                
                <!-- end video here -->
                <div style="clear:both;font-size:0px;line-height:0px;"><!--spacing div --></div></div>
              <div style="clear:both;font-size:0px;line-height:0px;"><!--spacing div --></div></div></td>
          </tr>
        </table></td>
      <td></td>
    </tr>
    <tr valign="top" bgcolor="#ffffff">
      <td></td>
      <td align="center" class="padtop"><table style="display: inline-table;" bgcolor="#009bda" border="0" cellpadding="0" cellspacing="0" width="908">
          <tr bgcolor="#FFFFFF">
            <td valign="top"><a href="index.php"><img name="car2gologo_s1" src="../imgs/car2go-logo_s1.jpg" width="136" height="95" border="0" id="car2gologo_s1" alt="" /></a><br /></td>
            <td valign="top"><a href="simple.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('simplenav_1','','../imgs/simple-nav_s2.jpg',1);"><img name="simplenav_1" src="../imgs/simple-nav_s1.jpg" width="142" height="95" border="0" id="simplenav_1" alt="" /></a></td>
            <td valign="top"><a href="convenient.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('convenientnav_1','','../imgs/convenient-nav_s2.jpg',1);"><img name="convenientnav_1" src="../imgs/convenient-nav_s1.jpg" width="145" height="95" border="0" id="convenientnav_1" alt="" /></a></td>
            <td valign="top"><a href="affordable.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('affordablenav_1','','../imgs/affordable-nav_s2.jpg',1);"><img name="affordablenav_1" src="../imgs/affordable-nav_s1.jpg" width="145" height="95" border="0" id="affordablenav_1" alt="" /></a></td>
            <td valign="top"><a href="sustainable.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('sustainablenav_1','','../imgs/sustainable-nav_s2.jpg',1);"><img name="sustainablenav_1" src="../imgs/sustainable-nav_s1.jpg" width="145" height="95" border="0" id="sustainablenav_1" alt="" /></a></td>
            <td valign="top" align="right" bgcolor="#FFFFFF" width="195"></td>
          </tr>
        </table></td>
      <td></td>
    </tr>
    <tr valign="top">
      <td width="21" height="21"><img src="../imgs/corner_left_bottom.png" width="21" height="21" alt="Portland Car Sharing"></td>
      <td bgcolor="#E3F5FC"></td>
      <td width="21" height="21"><img src="../imgs/corner_right_bottom.png" width="21" height="21" alt="Portland Car Sharing"></td>
    </tr>
  </table>
</div>
<?php require_once('footer.php'); ?>