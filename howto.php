<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>car2go Toronto | Toronto Car Share | Toronto car2go</title>
<meta name="description" content="Convenient car sharing in Toronto"/>
<meta id="MetaKeywords" name="KEYWORDS" content=" car2go Toronto, car to go, car sharing Toronto, car rentals, Toronto buses, car share" />
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<meta http-equiv="PRAGMA" content="NO-CACHE">
<meta http-equiv="Expires" content="0">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="imagetoolbar" content="no">
<link rel="shortcut icon" href="http://www.car2go.com/favicon.ico" type="image/x-icon">
<link rel="Stylesheet" type="text/css" href="style.css" title="Menu Styles">
<link rel="icon" href="http://www.car2go.com/favicon.ico" type="image/x-icon">
<script src="gen_validatorv4.js" type="text/javascript"></script>

<link rel="stylesheet" type="text/css" href="shadowbox.css">

<script type="text/javascript" src="shadowbox.js"></script>
<script type="text/javascript">
Shadowbox.init({
    modal: true
});
</script>
<script src="SpryAssets/SpryEffects.js" type="text/javascript"></script>
<script type="text/javascript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
function MM_effectAppearFade(targetElement, duration, from, to, toggle)
{
	Spry.Effect.DoFade(targetElement, {duration: duration, from: from, to: to, toggle: toggle});
}


<!--
var swf ='<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0" width="321" height="275"><param name="movie" value="car2go-HowItWorks2.swf"><param name="quality" value="high"><embed src="car2go-HowItWorks2.swf" width="321" height="275" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash"></embed></object>';

function flashSwap() 
{
	document.getElementById("movie").innerHTML = swf;
}
//-->
</script>
<style type="text/css" media="screen">
#flashContent {
	width:100%;
	height:100%;
}
</style>
</head>
<body id="index">
<div align="center">
  <table border="0" cellpadding="5" cellspacing="0" width="950">
    <tr>
      <td width="675">&nbsp;</td>
      <td align="right" valign="bottom"><iframe src="http://www.facebook.com/plugins/like.php?app_id=191729397539808&amp;href=http%3A%2F%2Fwww.facebook.com%2Fcar2go.austin&amp;send=false&amp;layout=button_count&amp;width=100&amp;show_faces=true&amp;action=like&amp;colorscheme=light&amp;font=arial&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:100px; height:21px;" allowTransparency="true"></iframe>
        
        <!-- AddThis Button BEGIN -->
        
    <?php require_once('add_this.php'); ?>
        <!-- AddThis Button END --></td>
    </tr>
  </table>
  <table border="0" cellpadding="0" cellspacing="0" width="950">
    <tr>
      <td width="21" height="21"><img src="imgs/corner_left_top.png" width="21" height="21" alt="Toronto Car Sharing"></td>
      <td bgcolor="#ffffff" width="908"></td>
      <td width="21" height="21"><img src="imgs/corner_right_top.png" width="21" height="21" alt="Toronto Car Sharing"></td>
    </tr>
    <tr>
      <td width="21" bgcolor="#FFFFFF"></td>
      <td bgcolor="#ffffff" width="908" style="padding-bottom:20px;"><table style="display: inline-table;" bgcolor="#009bda" border="0" cellpadding="0" cellspacing="0" width="908">
          <tr bgcolor="#FFFFFF">
            <td valign="top"><a href="index.php"><img name="car2gologo_s1" src="imgs/car2go-logo_s1.jpg" width="136" height="95" border="0" id="car2gologo_s1" alt="" /></a><br /></td>
            <td valign="top"><a href="simple.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('simplenav_s1','','imgs/simple-nav_s2.jpg',1);"><img name="simplenav_s1" src="imgs/simple-nav_s1.jpg" width="142" height="95" border="0" id="simplenav_s1" alt="" /></a></td>
            <td valign="top"><a href="affordable.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('affordablenav_s1','','imgs/affordable-nav_s2.jpg',1);"><img name="affordablenav_s1" src="imgs/affordable-nav_s1.jpg" width="145" height="95" border="0" id="affordablenav_s1" alt="" /></a></td>
            <td valign="top"><a href="sustainable.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('sustainablenav_s1','','imgs/sustainable-nav_s2.jpg',1);"><img name="sustainablenav_s1" src="imgs/sustainable-nav_s1.jpg" width="145" height="95" border="0" id="sustainablenav_s1" alt="" /></a></td>
            <td valign="top"><img name="howtonav_s1" src="imgs/howto-nav_s3.jpg" width="145" height="95" border="0" id="howtonav_s1" alt="" /></td>
            <td valign="top" align="right" bgcolor="#FFFFFF">&nbsp;</td>
          </tr>
        </table></td>
      <td width="21" bgcolor="#FFFFFF"></td>
    </tr>
    
    <tr valign="middle" bgcolor="#ffffff">
      <td>&nbsp;</td>
      <td valign="top">
      
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="21" height="21"><img src="imgs/blue_corner_topleft.gif" width="22" height="22" alt="Toronto Car Share"></td>
    <td height="21" bgcolor="#EBF5FA" width="100%">&nbsp;</td>
    <td width="21" height="21"><img src="imgs/blue_corner_topright.gif" width="22" height="22" alt="Toronto Car Share"></td>
  </tr>
</table>
      
      </td>
      <td>&nbsp;</td>
    </tr>
    <tr valign="middle" bgcolor="#ffffff">
      <td>&nbsp;</td>
      <td valign="top" bgcolor="#EBF5FA"><div class="leftside-subtxt"><img src="imgs/howto-head.jpg" width="309" height="75" alt="See How Car2Go Works">
<p>Watch a short video explaining how easy it is to grab a car2go and get going.</p></div>
        <div class="rightside-vid"><img src="imgs/video_placeholder.jpg" width="351" height="246" alt="See How Car2Go Works">
      </div></td>
      <td>&nbsp;</td>
    </tr>
    
    <tr valign="middle" bgcolor="#ffffff">
      <td>&nbsp;</td>
      <td valign="top">
      
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="21" height="21"><img src="imgs/blue_corner_bottomleft.gif" width="22" height="22" alt="Toronto Car Share"></td>
    <td height="21" bgcolor="#EBF5FA" width="100%">&nbsp;</td>
    <td width="21" height="21"><img src="imgs/blue_corner_bottomright.gif" width="22" height="22" alt="Toronto Car Share"></td>
  </tr>
</table>
      
      </td>
      <td>&nbsp;</td>
    </tr>
    
    <tr valign="top">
      <td width="950" colspan=3 valign="top" bgcolor="#ffffff"><div class="padtop padbot">
          <table cellpadding="0" cellspacing="0" border="0" width="950">
            <tr>
              <td width="21" bgcolor="#FFFFFF"></td>
              <td valign="top" style="padding-right:25px;"><a href="member.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('signuplink_s1','','imgs/signup-link_s2.jpg',1);"><img name="signuplink_s1" id="signuplink_s1" src="imgs/signup-link_s1.jpg" width="440" height="60" alt="Sign Up Now" border="0"></a></td>
              <td valign="top"><a href="drive.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('parkinginfolink_s1','','imgs/parkinginfo-link_s2.jpg',1);"><img name="parkinginfolink_s1" id="parkinginfolink_s1" src="imgs/parkinginfo-link_s1.jpg" width="440" height="60" alt="Parking Details" border="0"></a></td>
              <td width="21" bgcolor="#FFFFFF"></td>
            </tr>
          </table>
        </div></td>
    </tr>
<!-- Bottom Section Start -->
    <tr valign="middle" bgcolor="#E3F5FC">
      <td>&nbsp;</td>
      <td><div class="padtop"><table border="0" cellpadding="0" cellspacing="0" width="908"> 
          <tr>
            <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="21" height="21"><img src="imgs/wht_corner_topleft.gif" width="22" height="22" alt="Toronto Car Share"></td>
    <td height="21" bgcolor="#ffffff" width="100%">&nbsp;</td>
    <td width="21" height="21"><img src="imgs/wht_corner_topright.gif" width="22" height="22" alt="Toronto Car Share"></td>
  </tr>
</table>
</td>
          </tr>
          
          <tr>
            <td valign="top" align="left">
            	<div class="bottom-section">
                	<div class="bottom-left"><img src="imgs/findingacar2go.jpg" width="364" height="32" alt="Finding a Car2Go Toronto" border="0">
                    <ul class="bleft">
                    <li><a href="#">Download the app</a></li>
                    <li><a href="#">Check the map on our website</a></li>
                    <li><a href="#">Make a reservation online or be spontaneous. It’s up to you.</a></li>
                    </ul>
                  <a href="#"><img src="imgs/download_app.jpg" width="415" height="119" alt="Download Car2Go App" border="0"></a>
                  <p>&nbsp;</p>
                  <img src="imgs/whatitcosts.jpg" width="299" height="32" alt="What Car2Go Costs" border="0">
                  
                  <div class="chartsection">
                  	<div class="chart-header">Rental Rates<sup>1</sup></div>
                    	<div class="chart-row">
                    		<div class="chart-category">Registration Fee</div>
                    		<div class="chart-fee">$35</div>
                        </div>
                        <div class="chart-row">
                    		<div class="chart-category">Per Minute</div>
                    		<div class="chart-fee">$0.38</div>
                        </div>
                        <div class="chart-row">
                    		<div class="chart-category">Per Hour Maximum</div>
                    		<div class="chart-fee">$13.99</div>
                        </div>
                        <div class="chart-row">
                    		<div class="chart-category">Per Day Maximum</div>
                    		<div class="chart-fee">$72.99</div>
                        </div>
                        <div class="chart-row">
                    		<div class="chart-category">Per Mile After 150 Mile Per Rental</div>
                    		<div class="chart-fee">$0.48</div>
                        </div>
                        
                        <div class="note"><sup>1</sup>Lorem ipsum dolor sit amet.</div>
                        
                  </div>
                                    
                  </div>
                    <div class="bottom-right"><img src="imgs/thehomearea.jpg" width="320" height="32" alt="The Car2Go Home Area">
                    <a href="TorontoHomeArea.pdf"><img src="imgs/map_placeholder.jpg" width="351" height="417" alt="Home Area Map" border="0"></a>
                    <p>Begin and end your trip at any Green P parking lot inside the Home Area above or at any car2go specific parking location.<br><br>

You can drive outside the Home Area, but you cannot end your rental there. Click the map above for more information.<br><br>


For a more thorough look at parking locations and policies, check out our <a href="TorontoOnlineParking,pdf">parking details</a>.</p></div>
                </div>
            </td>
          </tr>
          
          <tr>
            <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="21" height="21"><img src="imgs/wht_corner_bottomleft.gif" width="22" height="22" alt="Toronto Car Share"></td>
    <td height="21" bgcolor="#ffffff">&nbsp;</td>
    <td width="21" height="21"><img src="imgs/wht_corner_bottomright.gif" width="22" height="22" alt="Toronto Car Share"></td>
    </tr>
</table>
</td>
          </tr>         
        </table></div></td>
      <td>&nbsp;</td>
<!-- Bottom Section End -->

    </tr>
    <tr valign="top">
      <td width="21" height="21"><img src="imgs/corner_left_bottom.png" width="21" height="21" alt="Toronto Car Sharing"></td>
      <td bgcolor="#E3F5FC"></td>
      <td width="21" height="21"><img src="imgs/corner_right_bottom.png" width="21" height="21" alt="Toronto Car Sharing"></td>
    </tr>
  </table>
</div>
<?php require_once('footer.php'); ?>